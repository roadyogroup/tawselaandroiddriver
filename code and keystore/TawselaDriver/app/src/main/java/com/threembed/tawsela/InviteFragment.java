package com.threembed.tawsela;
import java.util.ArrayList;
import java.util.List;
import com.app.driverapp.pojo.ContactsAdapPojo;
import com.app.driverapp.pojo.ContactsPojo;
import com.app.driverapp.utility.SessionManager;
import com.app.driverapp.utility.Utility;
import com.threembed.tawseladriver.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.support.v4.app.Fragment;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class InviteFragment extends Fragment 
{
	private  View view;
	private ArrayList<ContactsPojo> contact_list;
	private ArrayList<ContactsAdapPojo>rowItems;
	private ListView contact_listView;
	private String emailOrMessage[]={"Email","Message"};
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		SessionManager sessionManager = new SessionManager(getActivity());
		sessionManager.setIsFlagForOther(true);
//		HomeFragment.flagForHomeFragmentOpened = false;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) 
	{
		contact_list=new ArrayList<ContactsPojo>();
		
		
		if (view != null) 
		 {
		        ViewGroup parent = (ViewGroup) view.getParent();
		        if (parent != null)
		            parent.removeView(view);
		 }
		
		 try 
		    {
		        view = inflater.inflate(R.layout.invite_contact, container, false);
		    	
				
		    } catch (InflateException e)
		    {
		        /* map is already there, just return view as it is */
		    	Utility.printLog("", "onCreateView  InflateException "+e);
		    }
		    contact_listView=(ListView)view.findViewById(R.id.list_invite);
		    
		    
		new BackgroundReadContacts().execute();
		
		return view;
	}

	class BackgroundReadContacts extends AsyncTask<String,Void,String>
	{
		ProgressDialog dialogL;
		

		

		@Override
		protected void onPreExecute() 
		{
			super.onPreExecute();
			
			dialogL=Utility.GetProcessDialog(getActivity());
			
			if (dialogL!=null) {
				dialogL.show();
				dialogL.setCancelable(false);
			}
		}
		
		@Override
		protected String doInBackground(String... params) 
		{
			readContacts();
			return null;
		}
		
		private void readContacts() 
		{
			 ContentResolver cr =getActivity().getContentResolver();
			 Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
			
			 
			 while (cursor.moveToNext()) 
		     {
				 ContactsPojo userInfoObj=new ContactsPojo();
				 String  displayName,emailAddress, phoneNumber;
				 
				 //Name
				 displayName = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));     
				 userInfoObj.setContactName(displayName);
				 
				 //Email
				 String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
				 Cursor emails = cr.query(Email.CONTENT_URI,null,Email.CONTACT_ID + " = " + id, null, null);
		            
		           if(emails.moveToNext()) 
		            { 
		                emailAddress = emails.getString(emails.getColumnIndex(Email.DATA));
		                if (emailAddress!=null) {
		                	userInfoObj.setEmail_addr(emailAddress);
						}
		            }
		            emails.close();
				 
				 
		           //Phone
		            if(Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0)
		            {
		                Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",new String[]{id}, null);
		                //Log.i("", "readContacts cursor length pCur........"+pCur.getCount());
		               
		                if(pCur.moveToNext()) 
		                {
		                     phoneNumber = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
		                     phoneNumber=phoneNumber.replaceAll(" ", "");
		                     userInfoObj.setPh_numb(phoneNumber);
		                    
		                }
		                pCur.close();
		            }
		            //Add all to list
		            contact_list.add(userInfoObj); 
		     }
			 cursor.close();
		}

		@Override
		protected void onPostExecute(String result)
		{
			super.onPostExecute(result);
			
			if (dialogL!=null) {
				dialogL.dismiss();
			}
			
			rowItems=new ArrayList<ContactsAdapPojo>();
			
			for(int i=0;i<contact_list.size();i++)
			{
				ContactsAdapPojo item=new ContactsAdapPojo(contact_list.get(i).getContactName(),contact_list.get(i).getPh_numb(),contact_list.get(i).getEmail_addr());
				rowItems.add(item);
			}
			
			//java.util.Collections.sort(rowItems);
			java.util.Collections.sort(rowItems);
	        CustomAdapterInvite adapter = new CustomAdapterInvite(getActivity(), R.layout.contact_list_row, rowItems);
	        contact_listView.setAdapter(adapter);
			
		}
		
	}
	
	
	class CustomAdapterInvite extends ArrayAdapter<ContactsAdapPojo>
	{
		Context context;
		 Typeface robotoBoldCondensedItalic;
		public CustomAdapterInvite(Context context, int resourceId,
	            List<ContactsAdapPojo> items)
		{
	        super(context, resourceId, items);
	        this.context = context;
	         robotoBoldCondensedItalic = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Zurich Condensed.ttf");
	    }
		
		
		private class ViewHolder
		{
			TextView name;
			android.widget.ImageButton invite_btn;
			android.widget.RelativeLayout listviewmaillayout;
			/*TextView ph_numb;
			TextView email_id;*/
		}


		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			
			ViewHolder holder = null;
			final ContactsAdapPojo rowItem = getItem(position);
			
			LayoutInflater mInflater = (LayoutInflater) context
            .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			if (convertView == null)
			{
				convertView = mInflater.inflate(R.layout.contact_list_row, null);
	            holder = new ViewHolder();
				
				holder.name=(TextView)convertView.findViewById(R.id.contact_name);
	            holder.invite_btn=(ImageButton)convertView.findViewById(R.id.invite_btn);
	            holder.listviewmaillayout=(android.widget.RelativeLayout)convertView.findViewById(R.id.listviewmaillayout);
	            
				convertView.setTag(holder);
			}
			else
				holder = (ViewHolder) convertView.getTag();
			
			holder.listviewmaillayout.setOnClickListener(new View.OnClickListener()
			{
				
				@Override
				public void onClick(View v) 
				{
					selectOptionMenu(rowItem.getPh_numb(),rowItem.getEmail_addr());
				}
			});
			
			 holder.invite_btn.setOnClickListener(new View.OnClickListener()
			{
				
				@Override
				public void onClick(View v) 
				{
					selectOptionMenu(rowItem.getPh_numb(),rowItem.getEmail_addr());
				}
			});
			
			//Log.i("","null holder.name "+holder.name);
			//Log.i("","null rowItem.getContactName() "+rowItem.getContactName());
			 holder.name.setText(rowItem.getContactName());
			 holder.name.setTypeface(robotoBoldCondensedItalic);;
			return convertView;
		}
		
	}

	private void selectOptionMenu(final String phNumb,final String email_addr) {
		
		 AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		  builder.setTitle(getResources().getString(R.string.select));

		  builder.setItems(emailOrMessage, new DialogInterface.OnClickListener() 
		  {
			  			 
		   @Override
		   public void onClick(DialogInterface optiondialog, int which) 
		   {
			   if (emailOrMessage[which].equals(emailOrMessage[0])) 
			    {
				   Intent emailIntent = new Intent(Intent.ACTION_VIEW);
					Uri data = Uri
							.parse("mailto:?subject="
									+ "Invite to join"+getResources().getString(R.string.app_name) +"App!"
									+ "&body="
									+ "Please download the "+ getResources().getString(R.string.app_name)+ " www.roadyo.net"
									+ "&to=" 
									+ email_addr);
					
					emailIntent.setData(data);
					startActivity(emailIntent);
			    	optiondialog.dismiss();
			    }
			   if (emailOrMessage[which].equals(emailOrMessage[1])) 
			    {
				  /*startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("sms:Please download the SneekPeek App from www.sneekpeek.com"
	                        + phNumb)));*/
				   Intent intent = new Intent(Intent.ACTION_SENDTO);
				   intent.setData(Uri.parse("smsto:" + Uri.encode(phNumb)));
				   intent.putExtra("sms_body", "Please download the "+getResources().getString(R.string.app_name)+ " www.roadyo.net"); 
				   startActivity(intent);
			       optiondialog.dismiss();
			    }
		   }
		   });
		  builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
			    public void onClick(DialogInterface arg0, int arg1) {
			    	arg0.dismiss();
			    }
			});
		  AlertDialog  alert = builder.create();
		  alert.setCancelable(true);
		  alert.show();
		
	}

	

	

	
}
