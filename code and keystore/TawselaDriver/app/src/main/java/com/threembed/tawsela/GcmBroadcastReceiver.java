package com.threembed.tawsela;

import java.util.List;

import com.app.driverapp.utility.SessionManager;
import com.app.driverapp.utility.Utility;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.support.v4.content.WakefulBroadcastReceiver;
public class GcmBroadcastReceiver extends WakefulBroadcastReceiver
{
	private String message,action;
	public static final String PROPERTY_REG_ID = "registration_id";
	private static final String PROPERTY_APP_VERSION = "appVersion";

	@Override
	public void onReceive(Context context, Intent intent)
	{
		SessionManager session = new SessionManager(context);
		boolean bFlagForCurrent = session.isHomeIsOpend();
		Bundle extras = intent.getExtras();
		boolean isbackground = isApplicationSentToBackground(context);

		if (extras!=null) 
		{
			String str="";
			for(String key:intent.getExtras().keySet())
			{
				str=str+" "+key+"=>"+intent.getExtras().get(key) + ";";

			}
			Utility.printLog("Data is"+str);
		}

		if (extras!=null) 
		{
			if (extras!=null&&extras.toString().length()>0)
			{
				message = extras.getString("payload");
				action = extras.getString("action");

				session.setACTION(action);

				Utility.printLog("AAA action "+action);
				Utility.printLog("AAA extras "+extras);
			}
		}

		if (action==null||"com.google.android.c2dm.intent.REGISTRATION".equals(action))
		{
			Utility.printLog("C2DM", "Received registration ID");
			final String registrationId = intent.getStringExtra("registration_id");
			String error = intent.getStringExtra("error");

			Utility.printLog("C2DM", "dmControl: registrationId = " + registrationId+ ", error = " + error);
			storeRegistrationId(context, registrationId) ;
		}

		//getting live appointment

		else if ("7".equals(action)) 
		{
			Utility.printLog("Animation action inside GCM action 7");
			String aptDateTime = extras.getString("dt");
			String email=extras.getString("e");
			String bookingIdPush = extras.getString("bid");
			session.setAPT_DATE(aptDateTime);
			session.setPASSENGER_EMAIL(email);
			session.setBOOKING_ID(bookingIdPush);
			session.setFlagNewBooking(true);

			if (bFlagForCurrent)
			{
				Utility.printLog("Animation action inside GCM action 7 RACING");
				if (MainActivity.isResponse)
				{	
					Utility.printLog("Animation action inside GCM action 7 GCM GOT FIRST");
					MainActivity.isResponse= false;
					Intent homeIntent=new Intent("com.embed.anddroidpushntificationdemo11.push");
					Utility.printLog("extras="+extras);
					homeIntent.putExtras(extras);
					context.sendBroadcast(homeIntent);
				}
			}
			else if (!isbackground)
			{
				session.setIsRequested(true);
				Intent i = new Intent(context,MainActivity.class);
				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
				context.startActivity(i);
			}
			else
			{
				session.setIsRequested(true);
			}

		}
		else if ("51".equals(action)) 
		{
			Utility.printLog("Animation action inside GCM action 7");
			String aptDateTime = extras.getString("dt");
			String email=extras.getString("e");
			String bookingIdPush = extras.getString("bid");
			session.setAPT_DATE(aptDateTime);
			session.setPASSENGER_EMAIL(email);
			session.setBOOKING_ID(bookingIdPush);
			session.setFlagNewBooking(true);

			if (bFlagForCurrent)
			{
				Utility.printLog("Animation action inside GCM action 7 RACING");
				if (MainActivity.isResponse)
				{	
					Utility.printLog("Animation action inside GCM action 7 GCM GOT FIRST");
					MainActivity.isResponse= false;
					Intent homeIntent=new Intent("com.embed.anddroidpushntificationdemo11.push");
					Utility.printLog("extras="+extras);
					homeIntent.putExtras(extras);
					context.sendBroadcast(homeIntent);
				}
			}
			else if (!isbackground)
			{
				session.setIsRequested(true);
				Intent i = new Intent(context,MainActivity.class);
				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
				context.startActivity(i);
			}
			else
			{
				session.setIsRequested(true);
			}

		}

		//For cancel booking from passenger side
		else if ("10".equals(action))
		{
			session.setCancelPushFlag(true);
			if (!isbackground)
			{
				MainActivity.isResponse = true;
				Intent i = new Intent(context,MainActivity.class);
				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
				context.startActivity(i); 
			}
		}


		//For payment confirmation
		else if("11".equals(action))
		{
			session.setFlagForPayment(true);
			session.setPayload(message);
			if (bFlagForCurrent) 
			{
				Intent homeIntent=new Intent("com.embed.anddroidpushntificationdemo11.push");
				Utility.printLog("extras="+extras);
				homeIntent.putExtras(extras);
				context.sendBroadcast(homeIntent);				
			}
			else if (!isbackground)
			{
				Intent i = new Intent(context,MainActivity.class);
				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
				context.startActivity(i); 
			}
		}

		else if("12".equals(action))
		{
			session.setFlagForPayment(true);
			session.setPayload(message);
			if (bFlagForCurrent) 
			{
				Intent homeIntent=new Intent("com.embed.anddroidpushntificationdemo11.push");
				Utility.printLog("extras="+extras);
				homeIntent.putExtras(extras);
				context.sendBroadcast(homeIntent);				
			}
			else if (!isbackground)
			{
				Intent i = new Intent(context,MainActivity.class);
				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
				context.startActivity(i); 
			}
		}

		if (action != null ) {
			ComponentName comp = new ComponentName(context.getPackageName(), GcmIntentService.class.getName());
			startWakefulService(context, (intent.setComponent(comp)));
			setResultCode(Activity.RESULT_OK);
		}
	}

	public static boolean isApplicationSentToBackground(final Context context)
	{
		ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningTaskInfo> tasks = am.getRunningTasks(1);
		if (!tasks.isEmpty())
		{
			ComponentName topActivity = tasks.get(0).topActivity;
			if (!topActivity.getPackageName().equals(context.getPackageName())) 
			{
				return true;
			}
		}
		return false;
	}

	private void storeRegistrationId(Context context, String regId) 
	{
		final SharedPreferences prefs = getGCMPreferences(context);
		int appVersion = getAppVersion(context);

		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(PROPERTY_REG_ID, regId);
		editor.putInt(PROPERTY_APP_VERSION, appVersion);
		editor.commit();
	}

	private SharedPreferences getGCMPreferences(Context context) 
	{
		// This sample app persists the registration ID in shared preferences, but
		// how you store the regID in your app is up to you.
		return context.getSharedPreferences(LoginActivity.class.getSimpleName(),
				Context.MODE_PRIVATE);
	}
	public static int getAppVersion(Context context) 
	{
		try {
			PackageInfo packageInfo = context.getPackageManager()
					.getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

}
