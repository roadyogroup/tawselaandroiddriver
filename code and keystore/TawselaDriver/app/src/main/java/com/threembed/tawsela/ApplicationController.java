package com.threembed.tawsela;

import java.util.ArrayList;

import android.content.Intent;

import com.pubnub.api.Pubnub;
public class ApplicationController extends android.app.Application 
{
	private static Pubnub pubnub;
	private static ArrayList<String>ChanneList=new ArrayList<String>();
	//private static boolean IsCurrentStatusIsIAmOntheWay;
	private static Intent serviceIntent;
	@Override
	public void onCreate()
	{
		super.onCreate();
		serviceIntent=new Intent(this, MyService.class);
		pubnub = new Pubnub("pub-c-2776b5f1-eba3-44bc-ad08-21bcdbbd7f68", "sub-c-ee36ebe2-0ec6-11e5-a5c2-0619f8945a4f", "", true);
	}
	public static Pubnub getInstacePubnub()
	{
		return pubnub;
	}
	public static ArrayList<String> getChannelList()
	{
		return ChanneList;
	}
	public static Intent getMyServiceInstance()
	{
		return serviceIntent;
	}

}
