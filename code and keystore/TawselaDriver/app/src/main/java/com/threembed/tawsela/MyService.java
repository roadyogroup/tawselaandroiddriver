package com.threembed.tawsela;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;

import com.app.driverapp.utility.PublishUtility;
import com.app.driverapp.utility.SessionManager;
import com.app.driverapp.utility.Utility;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.pubnub.api.Pubnub;

public class MyService extends Service implements ConnectionCallbacks,OnConnectionFailedListener
{
	double mLatitude ;
	double mLongitude ; 
	float previousmLatitude = 0 ;
	float previousmLongitude = 0; 
	private Pubnub pubnub;
	private SessionManager sessionManager;
	//private int locationupdateCount = 0;
	private ArrayList<String>ChanneList ;
	private String subscribChannel;
	private String driverChannel;
	Timer myTimer_publish ;
	TimerTask myTimerTask_publish;
	double lati = 0.0 ,longi = 0.0;
	static double distancespd;
	double distanceKm;
	String strDouble;
	
	private GoogleApiClient mGoogleApiClient;
	private LocationRequest mLocationRequest;
	private Location myLoc;
	
	public MyService()
	{

	}
	//private LocationUpdate locationUpdate;
	@Override
	public IBinder onBind(Intent intent) 
	{
		throw new UnsupportedOperationException("Not yet implemented");
	}

	@Override
	public void onCreate() 
	{
		//Toast.makeText(this, "The new Service Created", Toast.LENGTH_SHORT).show();
		pubnub=ApplicationController.getInstacePubnub();
		//locationupdateCount=0;
		ChanneList=ApplicationController.getChannelList(); 
		sessionManager=new SessionManager(MyService.this);
		if (sessionManager.getBeginJourney()) 
		{
			distancespd = sessionManager.getDistanceInDouble();
		}
		else 
		{
			distancespd = 0.0;
		}

		driverChannel=sessionManager.getChannelName();	
		subscribChannel=sessionManager.getSubscribeChannel();
		ChanneList.clear();
		ChanneList.add(driverChannel);
		ChanneList.add(subscribChannel);
		buildGoogleApiClient();
	}

	@Override
	public void onStart(Intent intent, int startId) 
	{
		mGoogleApiClient.connect();
	}

	@Override
	public void onDestroy() 
	{
		mGoogleApiClient.disconnect();
		previousmLatitude = 0;
		previousmLongitude = 0;  
	}

	protected synchronized void buildGoogleApiClient() {
		mGoogleApiClient = new GoogleApiClient.Builder(this)
		.addConnectionCallbacks(this)
		.addOnConnectionFailedListener(this)
		.addApi(LocationServices.API)
		.build();
	}

	public void publishLocation(double latitude,double longitude)
	{
		//locationupdateCount=0;
		String driverName=sessionManager.getDriverName();
		if (sessionManager.isUserLogdIn())
		{
			for (int i = 0; i < ChanneList.size(); i++)
			{
				String message;
				if (sessionManager.getBeginJourney() && !"".equals(sessionManager.getBookingid())) 
				{
					message="{\"a\" :"+4+", \"e_id\" :\""+sessionManager.getUserEmailid()+"\",\"tp\" :\""+sessionManager.getVehTypeId()+"\", \"lt\" :"+latitude+", \"lg\" :"+longitude+",\"n\" :\""+driverName+"\",\"bid\" :\""+sessionManager.getBookingid()+"\", \"chn\" :\""+subscribChannel+"\"}";
				}
				else {
					message = "{\"a\" :"+4+", \"e_id\" :\""+sessionManager.getUserEmailid()+"\",\"tp\" :\""+sessionManager.getVehTypeId()+"\", \"lt\" :"+latitude+", \"lg\" :"+longitude+",\"n\" :\""+driverName+"\", \"chn\" :\""+subscribChannel+"\"}";
				}
				
				Utility.printLog("MyService","Message  " +message );
				Utility.printLog("MyService","channel name  " +ChanneList.get(i) );
				PublishUtility.publish(ChanneList.get(i),message,pubnub);
			}
		}
	}

	
	@Override
	public void onConnectionFailed(ConnectionResult result) 
	{
		
	}
	@Override
	public void onConnected(Bundle connectionHint)
	{
		mLocationRequest = LocationRequest.create();
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		mLocationRequest.setInterval(2000); // Update location every 2 second

		LocationServices.FusedLocationApi.requestLocationUpdates(
				mGoogleApiClient, mLocationRequest, new LocationListener() 
				{
					@Override
					public void onLocationChanged(Location location)
					{
						myLoc = location;
						//Utility.printLog("Location is"+location.toString());

						mLatitude = location.getLatitude();
						mLongitude = location.getLongitude(); 
						sessionManager.setDriverCurrentlat(""+mLatitude);
						sessionManager.setDriverCurrentLongi(""+mLongitude);
						if (sessionManager.getBeginJourney()) 
						{
							float mLatFloatValue = (float)mLatitude;
							float mLongFloatValue = (float)mLongitude;

							Location locationA = new Location("");     
							locationA.setLatitude(mLatFloatValue); 
							locationA.setLongitude(mLongFloatValue);
							if(lati == 0.0 && longi == 0.0)
							{
								lati = mLatFloatValue;
								longi = mLongFloatValue;
							}
							Location locationB = new Location("");
							locationB.setLatitude(lati);
							locationB.setLongitude(longi);
							double distanceChagne = locationA.distanceTo(locationB);
							distancespd += distanceChagne;
							sessionManager.setDistanceInDouble(""+distancespd);
							distanceKm = distancespd *(0.00062137);
							DecimalFormat df = new DecimalFormat("#.##");
							strDouble = df.format(distanceKm);
							Utility.printLog("Distance  = "+strDouble);
							sessionManager.setDistance(strDouble);
							lati = mLatFloatValue;
							longi = mLongFloatValue;
							if (sessionManager.getIsPassengerDropped())
							{
								sessionManager.setIsPassengerDropped(false);
								distancespd = 0.0;
							}
						}
						publishLocation(mLatitude,mLongitude);
					}
				});
		
		Handler postHandler = new Handler();
		
		postHandler.postDelayed(new Runnable() {
			
			@Override
			public void run() {
				if(myLoc==null)
				{
					Utility.printLog("NO LOCATION");
					//showSettingsAlert();
				}
			}
		}, 10000);
	}
	@Override
	public void onConnectionSuspended(int cause)
	{
		
	}
	/*public void showSettingsAlert()
	{
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

		// Setting Dialog Title
		alertDialog.setTitle(getResources().getString(R.string.gps));

		// Setting Dialog Message
		alertDialog.setMessage(getResources().getString(R.string.gpsdisable));

		// Setting Icon to Dialog
		//alertDialog.setIcon(R.drawable.delete);

		// On pressing Settings button
		alertDialog.setPositiveButton(getResources().getString(R.string.action_settings), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int which) {
				Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
				startActivity(intent);
			}
		});

		// on pressing cancel button
		alertDialog.setNegativeButton(getResources().getString(R.string.cancelbutton), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) 
			{
				dialog.cancel();
			}
		});

		// Showing Alert Message
		alertDialog.show();
	}*/
}