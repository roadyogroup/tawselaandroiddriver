package com.threembed.tawsela;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.driverapp.pojo.AppointmentDetailList;
import com.app.driverapp.response.PassengerAppointmentHistory;
import com.app.driverapp.response.PassengerAppointmentHistoryDetail;
import com.app.driverapp.utility.SessionManager;
import com.app.driverapp.utility.Utility;
import com.app.driverapp.utility.VariableConstants;
import com.app.driverapp.utility.VolleyErrorHelper;
import com.google.gson.Gson;
import com.threembed.tawseladriver.R;
public class HistoryActivity  extends android.support.v4.app.FragmentActivity implements android.widget.AdapterView.OnItemClickListener
{
	private android.widget.ListView historylistview;
	private AppointmentDetailList appointmentDetailList;
	private AppointmentHistoryListAdapter appointmentHistoryListAdapter;
	private BackGorundTaskForGetAppointmentHistory backGorundTaskForGetAppointmentHistory;
	private java.util.ArrayList<PassengerAppointmentHistoryDetail>appointmenthistorydetail=new java.util.ArrayList<PassengerAppointmentHistoryDetail>();
   @Override
protected void onCreate(android.os.Bundle arg0) 
   {
	          super.onCreate(arg0);
	        //opening transition animations
	  	       overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
			   setContentView(R.layout.historyactivity);
			   initActionBar();
				Bundle bundle=getIntent().getExtras();
				appointmentDetailList=	(AppointmentDetailList) bundle.getSerializable(VariableConstants.APPOINTMENT);
				intlayoutid();
				appointmentHistoryListAdapter=new AppointmentHistoryListAdapter(this, appointmenthistorydetail);
				historylistview.setAdapter(appointmentHistoryListAdapter);
				historylistview.setOnItemClickListener(this);
				getPatientHisroy();
   }
   
 /*  @Override
   public boolean onCreateOptionsMenu(android.view.Menu menu) 
   {
   	MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menutiemonpeteintdetail, menu);
   	return super.onCreateOptionsMenu(menu);
   }*/
   @Override
   public boolean onOptionsItemSelected(android.view.MenuItem item) 
   {
   	switch (item.getItemId()) 
   	{
   	 case android.R.id.home:
		  		// NavUtils.navigateUpFromSameTask(this);
		  		 finish();
		         return true;
		default:
			return super.onOptionsItemSelected(item);
		}
   	
   }
   
   
   
   @SuppressLint("NewApi")
private void initActionBar()
   {
	   ActionBar actionBar=getActionBar();
	   actionBar.setDisplayShowTitleEnabled(true);
       actionBar.setDisplayHomeAsUpEnabled(true);
       actionBar.setDisplayUseLogoEnabled(false);
  	 actionBar.setIcon(android.R.color.transparent);
		 actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.login_screen_navigation_bar));
		 try 
		 {
			 int actionBarTitle = Resources.getSystem().getIdentifier("action_bar_title", "id", "android");
			 TextView actionBarTitleView = (TextView) getWindow().findViewById(actionBarTitle);
			 Typeface robotoBoldCondensedItalic = Typeface.createFromAsset(getAssets(), "fonts/Zurich Condensed.ttf");
			 if(actionBarTitleView != null)
			 {
			     actionBarTitleView.setTypeface(robotoBoldCondensedItalic);
			     actionBarTitleView.setTextColor(android.graphics.Color.rgb(51, 51, 51));
			     
			 }
			 actionBar.setTitle(getResources().getString(R.string.history));
			// actionBarTitleView.setText();
		} catch (NullPointerException e)
		{
		}
		
   }
   
   
   
   private void intlayoutid()
   {
	   historylistview=(android.widget.ListView)findViewById(R.id.historylistview);
	   
   }
   private void getPatientHisroy()
   {
	   SessionManager sessionManager=new SessionManager(HistoryActivity.this);
			   //com.threembed.doctor.utilities.Utility utility=new com.threembed.doctor.utilities.Utility();
	   String sessionToken=sessionManager.getSessionToken();
	   String deviceid=Utility.getDeviceId(this);
	   String passengerEmailid=appointmentDetailList.getEmail();
	   String pageno="1";
	   Utility utility=new Utility();
		String currentdataandtimeingmt=utility.getCurrentGmtTime();
		//String currentdateandtimelocal=com.threembed.doctor.utilities.UltilitiesDate.getLocalTime(currentdataandtimeingmt);

	   
	   final String []mparams={sessionToken,deviceid,passengerEmailid,pageno,currentdataandtimeingmt};
	 //  backGorundTaskForGetAppointmentHistory=new BackGorundTaskForGetAppointmentHistory();
	 //  backGorundTaskForGetAppointmentHistory.execute(mparams);
	   
	   RequestQueue queue = Volley.newRequestQueue(this);  // this = context
	   final ProgressDialog mdialog;
	   mdialog=Utility.GetProcessDialog(HistoryActivity.this);
		mdialog.setMessage(getResources().getString(R.string.Pleasewaitmessage));
		mdialog.show();
	 String  url = VariableConstants.getAppointmentDetailhistory_url;
	   StringRequest postRequest = new StringRequest(Request.Method.POST, url,
	       new Response.Listener<String>()
	       {
	           @Override
	           public void onResponse(String response)
	           {
	               // response
	               Log.d("Response", response);
	                PassengerAppointmentHistory appointmentHistory;
	              //  Utility utility=new Utility();
	               
	                 //  List<NameValuePair>getAppointmentHistoryList;
	                //String appointHistroyResponse;
	           	Gson gson = new Gson();
	    		//Type type = new TypeToken<List<MasterAppointments>>(){}.getType();
	    		appointmentHistory = gson.fromJson(response, PassengerAppointmentHistory.class);
	     		
	    		
	    		try 
	    		{
	    			mdialog.dismiss();
	    			
	    			if (appointmentHistory.getErrFlag()==0&&appointmentHistory.getErrNum()==33) 
	    			{
	    				java.util.ArrayList<PassengerAppointmentHistoryDetail>historylist=appointmentHistory.getAppointmentHisttoryDetail();
	    				appointmenthistorydetail.clear();
	    				appointmenthistorydetail.addAll(historylist);
	    				appointmentHistoryListAdapter.notifyDataSetChanged();
	    				
	    			}
	    			else if (appointmentHistory.getErrFlag()==1&&appointmentHistory.getErrNum()==32) 
	    			{
	    				//32 -> (1) History not available.
	    				ErrorMessage(getResources().getString(R.string.messagetitle),appointmentHistory.getErrMsg(),true);
	    			}
	    			else if (appointmentHistory.getErrFlag()==1&&appointmentHistory.getErrNum()==7) 
	    			{
	    				//7 -> (1) Invalid token, please login or register.
	    				ErrorMessageForInvalidOrExpired(getResources().getString(R.string.messagetitle),appointmentHistory.getErrMsg());
	    			}
	    			
	    			else if (appointmentHistory.getErrFlag()==1&&appointmentHistory.getErrNum()==6) 
	    			{
	    				//6 -> (1) Session token expired, please login.
	    				ErrorMessageForInvalidOrExpired(getResources().getString(R.string.messagetitle),appointmentHistory.getErrMsg());
	    			}
	    			else if (appointmentHistory.getErrFlag()==1&&appointmentHistory.getErrNum()==1) 
	    			{
	    				//(1) Mandatory field missing
	    				ErrorMessage(getResources().getString(R.string.messagetitle),appointmentHistory.getErrMsg(),true);
	    			}
	    			
	    		}
	    		catch (Exception e) 
	    		{
	    			
	    			ErrorMessage(getResources().getString(R.string.messagetitle),getResources().getString(R.string.servererror),true);
	    		}
	    		
	    		
	    	
	    		
	           }
	       },
	       new Response.ErrorListener()
	       {
	    	 
	            @Override
	            public void onErrorResponse(VolleyError error)
	            {
	            	    mdialog.dismiss();
	            		String errorMessage = VolleyErrorHelper.getMessage(error, HistoryActivity.this);
	            		android.widget.Toast.makeText(HistoryActivity.this, errorMessage, android.widget.Toast.LENGTH_LONG);
	                // error
	                Log.d("Error.Response", error.toString());
	          }
	       }
	   ) {    
	       @Override
	       protected Map<String, String> getParams()
	       { 
	               Map<String, String>  params = new HashMap<String, String>(); 
	               params.put("ent_sess_token", mparams[0]); 
	               params.put("ent_dev_id", mparams[1]);
	               params.put("ent_pat_email", mparams[2]); 
	               params.put("ent_page", mparams[3]);
	               params.put("ent_date_time",mparams[4]);
	               
//	              namevaluepairs.add(new BasicNameValuePair("",));
//	     	      namevaluepairs.add(new BasicNameValuePair("",));
//	     	      namevaluepairs.add(new BasicNameValuePair("",));
//	     	      namevaluepairs.add(new BasicNameValuePair("",));
//	     	      namevaluepairs.add(new BasicNameValuePair("",));
	                
	               return params; 
	       }
	   };
	   int socketTimeout = 60000;//60 seconds - change to what you want
		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		postRequest.setRetryPolicy(policy);
	   queue.add(postRequest);
	   
	   
   }
   
   
   private class BackGorundTaskForGetAppointmentHistory extends android.os.AsyncTask<String , Void, PassengerAppointmentHistory>
   {
     private PassengerAppointmentHistory appointmentHistory;
     private Utility utility=new Utility();
     private  ProgressDialog mdialog;
     private    List<NameValuePair>getAppointmentHistoryList;
     private String appointHistroyResponse;
	@Override
	protected PassengerAppointmentHistory doInBackground(String... params)
	{
		getAppointmentHistoryList=utility.getAppointmentHistoryParameter(params);
		appointHistroyResponse=  utility.makeHttpRequest(VariableConstants.getAppointmentDetailhistory_url,VariableConstants.methodeName,getAppointmentHistoryList);
		//android.util.//Log.d("historyresponse", "BackGorundTaskForGetAppointmentHistory   doInBackground  appointHistroyResponse"+appointHistroyResponse);
		Gson gson = new Gson();
		//Type type = new TypeToken<List<MasterAppointments>>(){}.getType();
		appointmentHistory=gson.fromJson(appointHistroyResponse, PassengerAppointmentHistory.class);
		
		return appointmentHistory;
	}
	@Override
	protected void onPostExecute(PassengerAppointmentHistory result) 
	{
		super.onPostExecute(result);
		if (mdialog!=null)
		{
			mdialog.dismiss();
			mdialog.cancel();
			mdialog=null;
		}
		
	/*	Error Codes:
			ErrorNumber -> (ErrorFlag) ErrorMessage

			1 -> (1) Mandatory field missing
			6 -> (1) Session token expired, please login.
			7 -> (1) Invalid token, please login or register.
			32 -> (1) History not available.
			33 -> (0) Got the details!

			ErrorFlags: 0 – success, 1 – error*/
		/*"errNum":"7","errFlag":"7","errMsg":"Invalid token, please login or register.","test":103*/
		
		
		
		try 
		{
			if (result.getErrFlag()==0&&result.getErrNum()==33) 
			{
				java.util.ArrayList<PassengerAppointmentHistoryDetail>historylist=result.getAppointmentHisttoryDetail();
				appointmenthistorydetail.clear();
				appointmenthistorydetail.addAll(historylist);
				appointmentHistoryListAdapter.notifyDataSetChanged();
				
			}
			else if (result.getErrFlag()==1&&result.getErrNum()==32) 
			{
				//32 -> (1) History not available.
				ErrorMessage(getResources().getString(R.string.messagetitle),result.getErrMsg(),true);
			}
			else if (result.getErrFlag()==1&&result.getErrNum()==7) 
			{
				//7 -> (1) Invalid token, please login or register.
				ErrorMessageForInvalidOrExpired(getResources().getString(R.string.messagetitle),result.getErrMsg());
			}
			
			else if (result.getErrFlag()==1&&result.getErrNum()==6) 
			{
				//6 -> (1) Session token expired, please login.
				ErrorMessageForInvalidOrExpired(getResources().getString(R.string.messagetitle),result.getErrMsg());
			}
			else if (result.getErrFlag()==1&&result.getErrNum()==1) 
			{
				//(1) Mandatory field missing
				ErrorMessage(getResources().getString(R.string.messagetitle),result.getErrMsg(),true);
			}
			
		}
		catch (Exception e) 
		{
			ErrorMessage(getResources().getString(R.string.messagetitle),getResources().getString(R.string.servererror),true);
		}
		
		
	}
	
	
	@Override
	protected void onPreExecute() 
	{
		super.onPreExecute();
		mdialog=Utility.GetProcessDialog(HistoryActivity.this);
		mdialog.setMessage(getResources().getString(R.string.Pleasewaitmessage));
		mdialog.show();
	}
	   
   }
   
   

	 private void ErrorMessageForInvalidOrExpired(String title,String message)
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(HistoryActivity.this);
			builder.setTitle(title);
			builder.setMessage(message);
		 
			builder.setPositiveButton(getResources().getString(R.string.cancelbutton),
					new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					// Intent intent=new Intent(getActivity(), MainActivity.class);
	            	// startActivity(intent);
					dialog.dismiss();
				}
			});
			
			builder.setNegativeButton(getResources().getString(R.string.okbuttontext),
					new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					dialog.dismiss();
					Intent intent=new Intent(HistoryActivity.this, SplahsActivity.class);
				//	intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
				//	Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK
	               SessionManager sessionManager=new SessionManager(HistoryActivity.this);
	            	 sessionManager.logoutUser();
	            	 startActivity(intent);
	            	 finish();
					
				}
			});
			

			AlertDialog	 alert = builder.create();
			alert.setCancelable(false);
			alert.show();
		}
	
	
	 private void ErrorMessage(String title,String message,final boolean flageforSwithchActivity)
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(HistoryActivity.this);
			builder.setTitle(title);
			builder.setMessage(message);

			builder.setPositiveButton(getResources().getString(R.string.okbuttontext),
					new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					if (flageforSwithchActivity) 
					{
						
					}
					else
					{
						// only show message  
					}
					dialog.dismiss();
				}
			});

			AlertDialog	 alert = builder.create();
			alert.setCancelable(false);
			alert.show();
		}
   private class AppointmentHistoryListAdapter extends ArrayAdapter<PassengerAppointmentHistoryDetail>
   {
   	
   	
   	 private Activity mActivity;
   	// Typeface HelveticaLTStd_Light=Typeface.createFromAsset(getAssets(),"fonts/HelveticaLTStd-Light.otf");
       private LayoutInflater mInflater;
       private Utility utility=new Utility();
       private String ImageHosturl;
       Typeface robotoBoldCondensedItalic;
       
		public AppointmentHistoryListAdapter(Activity context,List<PassengerAppointmentHistoryDetail> objects) 
		{
			super(context, R.layout.appointmentlistitem, objects);
			mActivity=context;
			mInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			ImageHosturl = utility.getImageHostUrl(mActivity);
			robotoBoldCondensedItalic = Typeface.createFromAsset(mActivity.getAssets(), "fonts/Zurich Light Condensed.ttf");
		}

		@Override
		public int getCount() {
			return super.getCount();
		}

		@Override
		public PassengerAppointmentHistoryDetail getItem(int position) {
			return super.getItem(position);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			ViewHolder holder;
			if (convertView == null) 
			{
				holder = new ViewHolder();
				convertView = mInflater.inflate(R.layout.historylistitem, null);
		    	holder.textview=(TextView)convertView.findViewById(R.id.discription);
				holder.timeTextivew=(TextView)convertView.findViewById(R.id.timeTextivew);
				holder.phoneno=(TextView)convertView.findViewById(R.id.phoneno);
				
				convertView.setTag(holder);
				
			}
			else 
			{
				holder = (ViewHolder) convertView.getTag();
				//holder.imageview.setImageResource(R.drawable.multi_user_icon);
				
			}
			
			holder.textview.setId(position);
			//holder.imageview.setId(position);
			holder.timeTextivew.setId(position);
			holder.phoneno.setId(position);
			holder.timeTextivew.setText("Date and time   :"+getItem(position).getApptDt());
			holder.textview.setText("Notes :"+getItem(position).getRemarks());
		
			holder.textview.setTypeface(robotoBoldCondensedItalic);
			holder.timeTextivew.setTypeface(robotoBoldCondensedItalic);
			return convertView;
		}
		
		class ViewHolder 
		{
			//NetworkImageView imageview;
			TextView  textview;
			//TextView photocount;
			TextView timeTextivew;
			TextView phoneno;
			//RelativeLayout alubumitemimageViewParent;
		
		}
   	
   }
   
   @Override
   protected void onPause()
      {
   	super.onPause();
   	 //closing transition animations
       overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
   }

@Override
public void onItemClick(AdapterView<?> parent, View view, int position, long id)
{
	PassengerAppointmentHistoryDetail appointmentHisttoryDetail=(PassengerAppointmentHistoryDetail)parent.getItemAtPosition(position);
	Bundle bundle=new Bundle();
	bundle.putSerializable(VariableConstants.APPOINTMENT, appointmentHisttoryDetail);
	Intent intent=new Intent(HistoryActivity.this, HistoryDetailScreen.class);
	intent.putExtras(bundle);
	startActivity(intent);
	//bundle.putSerializable("HISTORYDETAILS", appointmentHisttoryDetail);
	
}
}
