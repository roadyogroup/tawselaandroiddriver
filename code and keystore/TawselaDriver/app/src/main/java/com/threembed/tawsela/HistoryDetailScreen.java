package com.threembed.tawsela;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.widget.TextView;
import com.threembed.tawseladriver.R;
import com.app.driverapp.response.PassengerAppointmentHistoryDetail;
import com.app.driverapp.utility.VariableConstants;


public class HistoryDetailScreen extends android.support.v4.app.FragmentActivity 
{
	
	private android.widget.TextView notetextview,notetextviewvalue;
   @Override
protected void onCreate(android.os.Bundle arg0) 
   {

	   super.onCreate(arg0);
	   setContentView(R.layout.historydetails);
	   overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
	   initActionBar();
	   android.os.Bundle extras=getIntent().getExtras();
	   intiLyoutid();
	   PassengerAppointmentHistoryDetail appointmentHisttoryDetail=(PassengerAppointmentHistoryDetail)extras.getSerializable(VariableConstants.APPOINTMENT);
	   notetextviewvalue.setText(appointmentHisttoryDetail.getRemarks());
	   Typeface robotoBoldCondensedItalic;
	   robotoBoldCondensedItalic = Typeface.createFromAsset(getAssets(), "fonts/Zurich Light Condensed.ttf");
	   notetextviewvalue.setTypeface(robotoBoldCondensedItalic);
	   notetextview.setTypeface(robotoBoldCondensedItalic);
   }
   @SuppressLint("NewApi")
private void initActionBar()
   {
	   ActionBar actionBar=getActionBar();
	   actionBar.setDisplayShowTitleEnabled(true);
       actionBar.setDisplayHomeAsUpEnabled(true);
       actionBar.setDisplayUseLogoEnabled(false);
       actionBar.setIcon(android.R.color.transparent);
	   actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.login_screen_navigation_bar));
		 try 
		 {
			 int actionBarTitle = Resources.getSystem().getIdentifier("action_bar_title", "id", "android");
			 TextView actionBarTitleView = (TextView) getWindow().findViewById(actionBarTitle);
			 Typeface robotoBoldCondensedItalic = Typeface.createFromAsset(getAssets(), "fonts/Zurich Condensed.ttf");
			 if(actionBarTitleView != null)
			 {
			     actionBarTitleView.setTypeface(robotoBoldCondensedItalic);
			     actionBarTitleView.setTextColor(android.graphics.Color.rgb(51, 51, 51));
			     
			 }
			 actionBar.setTitle(getResources().getString(R.string.history));
			// actionBarTitleView.setText();
		} catch (NullPointerException e)
		{
			// TODO: handle exception
		}
		
   }
   private void intiLyoutid()
   {
	   notetextview=(android.widget.TextView)findViewById(R.id.notetextview);
	   notetextviewvalue=(android.widget.TextView)findViewById(R.id.notetextviewvalue);
   }
   
   @Override
   public boolean onOptionsItemSelected(android.view.MenuItem item) 
   {
   	// TODO Auto-generated method stub
   	switch (item.getItemId()) 
   	{
   	 case android.R.id.home:
		  		// NavUtils.navigateUpFromSameTask(this);
		  		 finish();
		         return true;
		default:
			return super.onOptionsItemSelected(item);
		}
   	
   }
}
