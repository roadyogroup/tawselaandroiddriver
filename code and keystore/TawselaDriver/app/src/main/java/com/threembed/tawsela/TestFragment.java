package com.threembed.tawsela;
import com.app.driverapp.utility.Utility;
import com.threembed.tawseladriver.R;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public final class TestFragment extends Fragment 
{
    private static final String KEY_CONTENT = "TestFragment:Content";

    public static TestFragment newInstance(String content) 
    {
        TestFragment fragment = new TestFragment();

       /* StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 20; i++) 
        {
            builder.append(content).append(" ");
        }
        builder.deleteCharAt(builder.length() - 1);*/
        fragment.mContent =content/* builder.toString()*/;
        fragment.mContentint =Integer.parseInt(content)/* builder.toString()*/;
         android.util.Log.d("TestFragment", "TestFragment newInstance content "+content); 
        return fragment;
    }

    private String mContent = "????";
    private int mContentint = 0;
    private static View view;
    Typeface robotoBoldCondensedItalic;
    private Typeface robotregular;
    
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);

        if ((savedInstanceState != null) && savedInstanceState.containsKey(KEY_CONTENT))
        {
            mContent = savedInstanceState.getString(KEY_CONTENT);
        }
        Utility.printLog("TestFragment", "TestFragment onCreate mContent"+mContent); 
        	robotoBoldCondensedItalic = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Zurich Condensed.ttf");
        	robotregular= Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Regular.ttf");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) 
    {
    	
    	  
//      	if (view != null) 
//  		{
//  			ViewGroup parent = (ViewGroup) view.getParent();
//  			if (parent != null)
//  				parent.removeView(view);
//  		}
  		//try 
  		//{
  			view = inflater.inflate(R.layout.supportscreenlayout, null);
  			Utility.printLog("TestFragment", "onCreateView  view"+view); 

  		//} catch (InflateException e)
  		//{
  			/* map is already there, just return view as it is */
  			//Log.e("TestFragment", "onCreateView  InflateException "+e);
  		//}
    	
    	//android.util.Log.d("TestFragment", "TestFragment onCreateView "); 
    	//android.widget.ImageView imageView=new android.widget.ImageView(getActivity());
    	
    	
    
    	
//        TextView text = new TextView(getActivity());
//        text.setGravity(Gravity.CENTER);
//        text.setText(mContent);
//        text.setTextSize(20 * getResources().getDisplayMetrics().density);
//        text.setPadding(20, 20, 20, 20);

       // LinearLayout layout = new LinearLayout(getActivity());
       // layout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        //layout.setGravity(Gravity.CENTER);
        //layout.addView(imageView);
        initlayoutid(view,mContentint);
        return view;
    }
    
    private void initlayoutid(View view,int mContentint)
    {
    	android.widget.ImageView supportimageview=(android.widget.ImageView)view.findViewById(R.id.supportimageview);
    	android.widget.TextView textviewfirst=(android.widget.TextView)view.findViewById(R.id.textviewfirst);
    	android.widget.TextView textviewsecond=(android.widget.TextView)view.findViewById(R.id.textviewsecond);
    	android.widget.TextView textviewthird=(android.widget.TextView)view.findViewById(R.id.textviewthird);
    	//textviewfirst.setTextColor(android.graphics.Color.rgb(0, 102, 153));
    	
    	textviewfirst.setTypeface(robotoBoldCondensedItalic);
    	textviewsecond.setTypeface(robotregular);
    	textviewthird.setTypeface(robotregular);
    	
    	textviewsecond.setTextColor(android.graphics.Color.rgb(51, 51, 51));
    	textviewthird.setTextColor(android.graphics.Color.rgb(51, 51, 51));
    	switch (mContentint) 
    	{
    	case 1:
    		supportimageview.setImageResource(R.drawable.support_newbooking);
    		textviewfirst.setText(getResources().getString(R.string.newbooking));
    		textviewsecond.setText(getResources().getString(R.string.notifypassenger));
    		textviewthird.setText(getResources().getString(R.string.newbookingrequest));
    		break;
    	case 2:
    		supportimageview.setImageResource(R.drawable.support_arrive);
    		textviewfirst.setText(getResources().getString(R.string.driverarrived));
    		textviewsecond.setText(getResources().getString(R.string.notifypassengerwhen));
    		textviewthird.setText(getResources().getString(R.string.youarrived));
    		break;
    	case 3:
    		supportimageview.setImageResource(R.drawable.support_drop);
    		textviewfirst.setText(getResources().getString(R.string.passengerdrop));
    		textviewsecond.setText(getResources().getString(R.string.raiseandinvoceandgetpai));
    		textviewthird.setText(getResources().getString(R.string.ondroppingthepassenger));
    		break;
    	case 4:
    		supportimageview.setImageResource(R.drawable.support_rate);
    		textviewfirst.setText(getResources().getString(R.string.rate));
    		textviewsecond.setText(getResources().getString(R.string.helpustorateby));
    		textviewthird.setText(getResources().getString(R.string.ourcustomers));
    		
    		break;
    	/*case 4:
    		supportimageview.setImageResource(R.drawable.support_bookprivemd);
    		textviewfirst.setText(getResources().getString(R.string.notessupport));
    		textviewsecond.setText(getResources().getString(R.string.takeantesforyour));
    		textviewthird.setText(getResources().getString(R.string.penddingappointment));
    		
    		break;
*/
    	default:
    		break;
    	}
    	
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_CONTENT, mContent);
    }
}
