package com.threembed.tawsela;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.ParseException;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.driverapp.pojo.AppointmentDetailList;
import com.app.driverapp.response.AppointmentDetailData;
import com.app.driverapp.response.UpdateAppointMentstatus;
import com.app.driverapp.utility.ConnectionDetector;
import com.app.driverapp.utility.NetworkConnection;
import com.app.driverapp.utility.PublishUtility;
import com.app.driverapp.utility.Scaler;
import com.app.driverapp.utility.SessionManager;
import com.app.driverapp.utility.Utility;
import com.app.driverapp.utility.VariableConstants;
import com.google.gson.Gson;
import com.pubnub.api.Pubnub;
import com.threembed.tawseladriver.R;

@SuppressLint("SimpleDateFormat")
public class JourneyDetailsActivity extends FragmentActivity
{
	private TextView /*passengerName,*/actionbar_textview,appntDate,pickupLocation,dropoffLocation,distance,pick_time,drop_time,total_time,approx_fare,waiting_time;
	private Button finish,finish_button;	
	//private TextView cs1,cs2,cs3,cs4,cs5;
	private RelativeLayout network_bar;
	private TextView network_text,totalFare;
	//private ImageView detailimageview;
	//private EditText toll_tax_fare,parking_tax_fare,airport_tax_fare,meter_tax_fare;
	private ActionBar actionBar;
	private AppointmentDetailList appointmentDetailList;
	private AppointmentDetailData appointmentDetailData;
	private int selectedindex;
	private int selectedListIndex;
	private ProgressDialog mdialog;
	private Pubnub pubnub;
	private String currencySymbol = VariableConstants.CURRENCY;
	private RatingBar ratingBar;
	String amount;
	private SessionManager sessionManager;
	//private double totalFareDouble = 0.0;
	private Timer myTimer_publish;
	private TimerTask myTimerTask_publish;
	private BroadcastReceiver receiver;
	private IntentFilter filter;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle arg0) 
	{
		super.onCreate(arg0);
		setContentView(R.layout.journey_detail);
		overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
		pubnub=ApplicationController.getInstacePubnub();
		initLayoutId();
		sessionManager = new SessionManager(this);
		sessionManager.setDropAddress(getCompleteAddressString(sessionManager.getDriverCurrentLat(), sessionManager.getDriverCurrentLongi()));
		sessionManager.setIsFlagForOther(true);
		sessionManager.setDistance(""+0.0);
		Utility utility=new Utility();
		initActionBar();
		/*actionBar = getActionBar();
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setDisplayHomeAsUpEnabled(false);
		actionBar.setDisplayUseLogoEnabled(false);*/
		MainActivity.isResponse=true;
		//getLocation(JourneyDetailsActivity.this);
		Bundle bundle=getIntent().getExtras();
		
		filter = new IntentFilter();
		filter.addAction("com.app.driverapp.internetStatus");
		receiver = new BroadcastReceiver()
		{
			@Override
			public void onReceive(Context context, Intent intent)
			{
				try 
				{
					Bundle bucket=intent.getExtras();
					
					String status = bucket.getString("STATUS");

					if(status.equals("1"))
					{
						network_bar.setVisibility(View.GONE);
					}
					else
					{
						if (!Utility.isNetworkAvailable(JourneyDetailsActivity.this))
						{
							network_bar.setVisibility(View.VISIBLE);
							return;
						}
						else if (!NetworkConnection.isConnectedFast(JourneyDetailsActivity.this)) 
						{
							network_bar.setVisibility(View.VISIBLE);
							network_text.setText(getResources().getString(R.string.lownetwork));
							return;
						}
					}
				} 
				catch (Exception e)
				{
					Utility.printLog("BroadcastReceiver Exception "+e);
				}
			}
		};
		
		/*actionBar.setIcon(android.R.color.transparent);
		actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#333333")));
		//Typeface robotoBoldCondensedItalic = Typeface.createFromAsset(getAssets(), "fonts/Zurich Condensed.ttf");
		try 
		{
			int actionBarTitle = Resources.getSystem().getIdentifier("action_bar_title", "id", "android");
			TextView actionBarTitleView = (TextView) getWindow().findViewById(actionBarTitle);

			if(actionBarTitleView != null)
			{
				//actionBarTitleView.setTypeface(robotoBoldCondensedItalic);
				//actionBarTitleView.setTextColor(android.graphics.Color.rgb(51, 51, 51));
			}
			actionBar.setTitle(getResources().getString(R.string.finish));
			// actionBarTitleView.setText();
		} 
		catch (NullPointerException e)
		{
			Utility.printLog("NullPointerException"+e);
		}*/
		/*try 
		{
			Geocoder geocoder = new Geocoder(this, Locale.getDefault());
			List<Address> addresses;
			addresses = geocoder.getFromLocation(sessionManager.getDriverCurrentLat(), sessionManager.getDriverCurrentLongi(), 1);
			Address address = addresses.get(0);
			currencySymbol = Currency.getInstance(address.getLocale()).getSymbol();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}*/

		//finish.setTypeface(robotoBoldCondensedItalic);
		appointmentDetailList = (AppointmentDetailList) bundle.getSerializable(VariableConstants.APPOINTMENT);
		appointmentDetailData = appointmentDetailList.getAppointmentDetailData();
		Utility.printLog(" AAAAADistanceeeeeeee   == "+appointmentDetailData.getDis());
		/*double arrayOfscallingfactor[]=Scaler.getScalingFactor(JourneyDetailsActivity.this);
		double width = (160)*arrayOfscallingfactor[0];
		double height = (150)*arrayOfscallingfactor[1];
		String imageUrl =utility.getImageHostUrl(this) + appointmentDetailData.getpPic();
		Picasso.with(this) //
		.load(imageUrl) //
		.placeholder(R.drawable.ic_launcher) //
		.error(R.drawable.ic_launcher).fit()
		.resize((int)Math.round(width),(int)Math.round(height))	 //
		.into(detailimageview); */
		//passengerName.setText(appointmentDetailData.getfName());
		if (currencySymbol != null) 
		{
			/*cs1.setText(currencySymbol);
			cs2.setText(currencySymbol);
			cs3.setText(currencySymbol);
			cs4.setText(currencySymbol);
			cs5.setText(currencySymbol);*/
			
			approx_fare.setText(currencySymbol +" "+sessionManager.getAPX_AMOUNT());
		}
		
		
		selectedindex = bundle.getInt("selectedindex");
		selectedListIndex = bundle.getInt("horizontapagerIndex");

		//meter_tax_fare.setText(sessionManager.getAPX_AMOUNT());
		
		waiting_time.setText(sessionManager.getWaitingTime());

		

		try 
		{
			String datestr = sessionManager.getBeginTime();
			String date_string = datestr.toString();
			String[] temp1 = date_string.split(" ");
			String[] temp2=temp1[1].split(":");
			String final_date = null;
			if(Integer.parseInt(temp2[0])>12)
			{
				int temp_hh=Integer.parseInt(temp2[0])-12;
				final_date=""+temp_hh+":"+temp2[1]+" PM";
			}
			else
			{
				final_date=temp2[0]+":"+temp2[1]+" AM";
			}
			pick_time.setText(""+final_date);

			String droptime = utility.getCurrentGmtTime();
			date_string = droptime.toString();
			temp1 = date_string.split(" ");
			temp2 = temp1[1].split(":");
			Utility.printLog("", "final_date: " + final_date);

			if(Integer.parseInt(temp2[0])>12)
			{
				int temp_hh=Integer.parseInt(temp2[0])-12;
				final_date=""+temp_hh+":"+temp2[1]+" PM";
			}
			else
			{
				final_date=temp2[0]+":"+temp2[1]+" AM";
			}
			drop_time.setText(""+final_date);

			if (!"".equals(sessionManager.getDuration())){
				String []time = sessionManager.getDuration().split(":");

				total_time.setText(""+time[0]+"H :"+time[1]+"M");
			}

			/*String dateString = appointmentDetailData.getApptDt();
			String[] parts = dateString.split(" ");
			String part1 = parts[0];


			SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			Utility.printLog("AAAA datestr"+datestr);
			Utility.printLog("AAAA droptime"+droptime);
			java.util.Date stardDate=sd.parse(datestr);
			java.util.Date endDate=sd.parse(droptime);
			long msDiff = endDate.getTime() - stardDate.getTime();
			if(msDiff>0)
			{
				long totalSeconds = (msDiff)/1000;
				long seconds  = totalSeconds%60;
				long Minute = (totalSeconds/60)%60;
				long Hours = (totalSeconds/(60*60))%(24);
				//long Days= totalSeconds/(60*60*24);
				total_time.setText(""+Hours+" H :"+Minute+" M :"+seconds+" S");
			}*/
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		pickupLocation.setText(appointmentDetailData.getAddr1());
		dropoffLocation.setText(sessionManager.getDropAddress());
		distance.setText(sessionManager.getDistance_tag()+" "+getResources().getString(R.string.km));
		//avg_spd.setText(sessionManager.getAVG_SPEED()+" "+getResources().getString(R.string.kmh));

		//startTimerToGetFares();

	}

	@Override
	public void onBackPressed() 
	{
		super.onBackPressed();
		finish();
	}
	
	private void initLayoutId()
	{
		//passengerName = (TextView)findViewById(R.id.passenger_name_text);
		appntDate = (TextView)findViewById(R.id.date_text);
		/*cs1 = (TextView)findViewById(R.id.cs1);
		cs2 = (TextView)findViewById(R.id.cs2);
		cs3 = (TextView)findViewById(R.id.cs3);
		cs4 = (TextView)findViewById(R.id.cs4);
		cs5 = (TextView)findViewById(R.id.cs5);*/
		//totalFare = (TextView)findViewById(R.id.total_tax_fare);
		//toll_tax_fare = (EditText)findViewById(R.id.toll_tax_fare);
		//parking_tax_fare = (EditText)findViewById(R.id.parking_tax_fare);
		//airport_tax_fare = (EditText)findViewById(R.id.airport_tax_fare);
		//meter_tax_fare = (EditText)findViewById(R.id.meter_tax_fare);
		network_bar = (RelativeLayout)findViewById(R.id.network_bar);
		network_text = (TextView)findViewById(R.id.network_text);
		pickupLocation = (TextView)findViewById(R.id.pickup_address);
		dropoffLocation = (TextView)findViewById(R.id.dropoff_address);
		approx_fare = (TextView)findViewById(R.id.total_amount);
		distance = (TextView)findViewById(R.id.distance);
		pick_time = (TextView)findViewById(R.id.pickup_time);
		drop_time = (TextView)findViewById(R.id.dropoff_time);
		total_time = (TextView)findViewById(R.id.total_time);
		waiting_time = (TextView)findViewById(R.id.avg_spd);
		ratingBar = (RatingBar)findViewById(R.id.invoice_driver_rating);
		finish_button = (Button)findViewById(R.id.finish_button);
		finish_button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) 
			{
				ConnectionDetector connectionDetector=new ConnectionDetector(JourneyDetailsActivity.this);
				if (connectionDetector.isConnectingToInternet()) 
				{
					sendNotificationToPassenger(9);
				}
				
			}
		});
		
	}

	
	@SuppressLint({ "NewApi", "InflateParams" })
	private void initActionBar()
	{
		actionBar = getActionBar();
		actionBar.setHomeButtonEnabled(false);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayHomeAsUpEnabled(false);
		actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#333333")));
		LayoutInflater inflater =(LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		RelativeLayout mActionBarCustom = (RelativeLayout)inflater.inflate(R.layout.custom_actionbar, null);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setIcon(android.R.color.transparent);
		actionbar_textview=(TextView)mActionBarCustom.findViewById(R.id.actionbar_textview);
		finish = (Button)mActionBarCustom.findViewById(R.id.cancel);
		finish.setText("");
		/*finish.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{

				ConnectionDetector connectionDetector=new ConnectionDetector(JourneyDetailsActivity.this);
				if (connectionDetector.isConnectingToInternet()) 
				{
					amount = meter_tax_fare.getText().toString();
					if (!(amount.endsWith(".")) && !(amount.equals(currencySymbol+"0")) && !(amount.equals(currencySymbol)) && !(amount.startsWith(currencySymbol+".")) && !(amount.equals("0")) && !(amount.isEmpty()) ) 
					{
						sessionManager.setBeginJourney(false);
						sendNotificationToPassenger(9);
					}
					else
					{
						showAlert(getResources().getString(R.string.entervalidamount));
					}
				}
			
			}
		});*/
		actionbar_textview.setText(getResources().getString(R.string.finish));
		actionbar_textview.setGravity(Gravity.CENTER);
		double scaler[]=Scaler.getScalingFactor(this);
		int padding = (int)Math.round(140*scaler[0]);
		actionbar_textview.setPadding(padding, 0,0,0);
		actionBar.setCustomView(mActionBarCustom);
	}
	
	@Override
	protected void onResume() 
	{
		super.onResume();
		if (receiver != null) 
		{
			registerReceiver(receiver, filter);
		}
	}
	
	@Override
	protected void onPause() 
	{
		super.onPause();
		unregisterReceiver(receiver);
	}


	/**
	 * Method for updating appointment status  
	 * @param responsecode
	 */
	private void sendNotificationToPassenger(final int responsecode)
	{
		Utility utility=new Utility();

		ConnectionDetector connectionDetector=new ConnectionDetector(JourneyDetailsActivity.this);
		if (connectionDetector.isConnectingToInternet()) 
		{
			String deviceid = Utility.getDeviceId(JourneyDetailsActivity.this);
			String currenttime = utility.getCurrentGmtTime();
			SessionManager sessionManager=new SessionManager(JourneyDetailsActivity.this);
			//logDebug("getAppointmentDetail dataandTime "+dateandTime);
			String passengerEmailid = appointmentDetailData.getEmail();
			float ratingBarValue = ratingBar.getRating();
			String appointdatetime = appointmentDetailData.getApptDt();
			String sessiontoken = sessionManager.getSessionToken();
			String notes="";
			String parking_fare ="00.00";
			String toll_fare = "00.00";
			String airport_fare = "00.00"; 
			String amount = sessionManager.getAPX_AMOUNT();
			double amount_double = Double.parseDouble(amount);

			/*if (!"".equals(parking_tax_fare.getText().toString())) 
			{
				parking_fare = parking_tax_fare.getText().toString();
			}*/

			/*if (!"".equals(toll_tax_fare.getText().toString())) 
			{
				toll_fare = toll_tax_fare.getText().toString();
			}*/
			


			/*if (!"".equals(airport_tax_fare.getText().toString())) 
			{
				airport_fare = airport_tax_fare.getText().toString();
			}*/

			
			final String mparams[]={sessiontoken,deviceid,passengerEmailid,appointdatetime,""+responsecode,""+amount_double ,notes,currenttime,/*dropAddress,*/""+ratingBarValue/*currentdate[0]*/,parking_fare,toll_fare,airport_fare};
			mdialog = Utility.GetProcessDialog(JourneyDetailsActivity.this);
			mdialog.setMessage(getResources().getString(R.string.Pleasewaitmessage));
			mdialog.show();
			mdialog.setCancelable(false);
			RequestQueue queue = Volley.newRequestQueue(this);  // this = context
			String  url = VariableConstants.getAppointmentstatusUpdate_url;
			StringRequest postRequest = new StringRequest(Request.Method.POST, url,
					new Response.Listener<String>()
					{
				@Override
				public void onResponse(String response)
				{
					try 
					{
						Utility.printLog("sendNotificationResponse9"+response);
						UpdateAppointMentstatus appointMentstatus;
						Gson gson = new Gson();
						appointMentstatus = gson.fromJson(response, UpdateAppointMentstatus.class);
						Utility.printLog("Journey Detail sendNotification9"+response);

						if (mdialog!=null)
						{
							mdialog.dismiss();
							mdialog.cancel();
						}
						// 1 -> (1) Mandatory field missing
						if (appointMentstatus.getErrFlag()==0 && appointMentstatus.getErrNum() == 59)
						{
							appointmentDetailList.setStatCode(9);					
							SessionManager sessionManager=new SessionManager(JourneyDetailsActivity.this);
							sessionManager.setIsAppointmentAccept(false);
							sessionManager.setIsPressedImonthewayorihvreached(false);
							sessionManager.setIsPassengerDropped(true);
							sessionManager.setBookingid("");
							sessionManager.setindexofSelectedAppointment(selectedindex);
							sessionManager.setindexofSelectedList(selectedListIndex);
							sessionManager.setAppiontmentStatus(responsecode);
							appointmentDetailList.setCompletedPressed(true);
							appointmentDetailList.setIhaveReachedPressed(true);
							sessionManager.setFlagForStatusDropped(false);
							
							Utility.printLog("Latitude  = "+sessionManager.getDriverCurrentLat(),"Longitude = "+sessionManager.getDriverCurrentLongi());
							for (int i = 0; i < 5; i++) 
							{
								publishLocation(sessionManager.getDriverCurrentLat(),sessionManager.getDriverCurrentLongi());
							}
							android.widget.Toast.makeText(JourneyDetailsActivity.this, appointMentstatus.getErrMsg(), android.widget.Toast.LENGTH_SHORT).show();
							Intent intent=new Intent(JourneyDetailsActivity.this, MainActivity.class);
							sessionManager.setIsOnButtonClicked(true);
							Bundle bundle=new Bundle();
							bundle.putSerializable(VariableConstants.APPOINTMENT, (Serializable) appointmentDetailList);
							intent.putExtras(bundle);
							startActivity(intent);
							finish();
						}
						else if (appointMentstatus.getErrFlag()==1&&appointMentstatus.getErrNum()==56)
						{
							// 56 -> (1) Invalid status, cannot update.
							ErrorMessage(getResources().getString(R.string.messagetitle),appointMentstatus.getErrMsg(),false);
						}
						
						else if (appointMentstatus.getErrFlag()==1&&appointMentstatus.getErrNum()==95)
						{
							// 56 -> (1) Invalid status, cannot update.
							ErrorMessage(getResources().getString(R.string.messagetitle),appointMentstatus.getErrMsg(),false);
						}

						else if (appointMentstatus.getErrFlag()==1&&appointMentstatus.getErrNum()==3)
						{
							// 3 -> (1) Error occurred while processing your request.
							ErrorMessage(getResources().getString(R.string.messagetitle),appointMentstatus.getErrMsg(),false);
						}

						else if (appointMentstatus.getErrFlag()==1&&appointMentstatus.getErrNum()==6)
						{
							// 6 -> (1) Session token expired, please login.
							ErrorMessageForInvalidOrExpired(getResources().getString(R.string.messagetitle),appointMentstatus.getErrMsg());
						}
						else if (appointMentstatus.getErrFlag()==1&&appointMentstatus.getErrNum()==7)
						{
							// 7 -> (1) Invalid token, please login or register.
							ErrorMessageForInvalidOrExpired(getResources().getString(R.string.messagetitle),appointMentstatus.getErrMsg());
						}
						else if (appointMentstatus.getErrFlag()==1&&appointMentstatus.getErrNum()==1)
						{
							// 1 -> (1) Mandatory field missing
							ErrorMessage(getResources().getString(R.string.messagetitle),appointMentstatus.getErrMsg(),false);
						}
						else if (appointMentstatus.getErrFlag()==1&&appointMentstatus.getErrNum()==96)
						{
							// 1 -> (1) Mandatory field missing
							ErrorMessage(getResources().getString(R.string.messagetitle),appointMentstatus.getErrMsg(),false);
						}
						else if (appointMentstatus.getErrFlag()==1)
						{
							// 1 -> (1) Mandatory field missing
							ErrorMessage(getResources().getString(R.string.messagetitle),appointMentstatus.getErrMsg(),false);
						}
					} 
					catch (Exception e) 
					{
						Utility.printLog("ExceptionException"+e);
						//ErrorMessage(getResources().getString(R.string.messagetitle),getResources().getString(R.string.servererror),false);
					}
				}
					},
					new Response.ErrorListener()
					{
						@Override
						public void onErrorResponse(VolleyError error) 
						{
							if (mdialog!=null)
							{
								mdialog.dismiss();
								mdialog.cancel();
							}
							ErrorMessage(getResources().getString(R.string.messagetitle),getResources().getString(R.string.servererror),false);
						}
					}
					) {    
				@Override
				protected Map<String, String> getParams()
				{ 
					Map<String, String>  params = new HashMap<String, String>(); 
					params.put("ent_sess_token",mparams[0]); 
					params.put("ent_dev_id",mparams[1]);

					params.put("ent_pas_email", mparams[2]); 
					params.put("ent_appnt_dt",mparams[3]);

					params.put("ent_response", mparams[4]); 
					params.put("ent_amount",mparams[5] );
					params.put("ent_doc_remarks", mparams[6]);
					params.put("ent_date_time", mparams[7]);
					//params.put("ent_drop_addr_line1",mparams[8]);
					params.put("ent_rating",mparams[8]);

					params.put("ent_parking",mparams[9] );
					params.put("ent_toll", mparams[10]); 
					params.put("ent_airport", mparams[11]);

					Utility.printLog("get Notification request9 = "+params);

					return params; 
				}
			};
			int socketTimeout = 60000;//60 seconds - change to what you want
			RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
			postRequest.setRetryPolicy(policy);
			queue.add(postRequest);
		}
		else 
		{
			utility.showDialogConfirm(JourneyDetailsActivity.this,getResources().getString(R.string.note),getResources().getString(R.string.nonetwork), false).show();
		}
	}

	private void ErrorMessage(String title,String message,final boolean flageforSwithchActivity)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(JourneyDetailsActivity.this);
		builder.setTitle(title);
		builder.setMessage(message);

		builder.setPositiveButton(getResources().getString(R.string.okbuttontext),
				new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				if (flageforSwithchActivity) 
				{
					Intent intent = new Intent(JourneyDetailsActivity.this,MainActivity.class);
					startActivity(intent);
					finish();
				}
				else
				{
					// only show message 
					dialog.dismiss();
				}

			}
		});

		AlertDialog	 alert = builder.create();
		alert.setCancelable(false);
		alert.show();
	}

	private void ErrorMessageForInvalidOrExpired(String title,String message)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(JourneyDetailsActivity.this);
		builder.setTitle(title);
		builder.setMessage(message);

		builder.setPositiveButton(getResources().getString(R.string.cancelbutton),
				new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
			}
		});

		builder.setNegativeButton(getResources().getString(R.string.okbuttontext),
				new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				SessionManager sessionManager=new SessionManager(JourneyDetailsActivity.this);
				sessionManager.logoutUser();
				dialog.dismiss();
				Intent intent=new Intent(JourneyDetailsActivity.this, SplahsActivity.class);
				startActivity(intent);
				finish();
			}
		});

		AlertDialog	 alert = builder.create();
		alert.setCancelable(false);
		alert.show();
	}

	/**
	 * Method for publish current location to passenger. 
	 * @param latitude
	 * @param longitude
	 */
	public void publishLocation(double latitude,double longitude)
	{
		String message;
		SessionManager sessionManager = new SessionManager(this);
		String subscribChannel=sessionManager.getSubscribeChannel();
		message="{\"a\" :\""+9+"\", \"e_id\" :\""+sessionManager.getUserEmailid()+"\", \"lt\" :"+latitude+"\", \"lg\" :"+longitude+"\", \"ph\" :\""+sessionManager.getMobile()+"\",\"dt\" :\""+sessionManager.getDate()+"\",\"bid\" :\""+sessionManager.getBOOKING_ID()+"\",\"chn\" :\""+subscribChannel+"\"}";
		Utility.printLog("Publish Location = "+message);

		if (sessionManager.getPasChannel() != null)
		{
			Utility.printLog("Publish Passenger Channel"+sessionManager.getPasChannel());
			PublishUtility.publish(sessionManager.getPasChannel(), message, pubnub);
		}
		else
		{
			ErrorMessage(getResources().getString(R.string.messagetitle),getResources().getString(R.string.passengercancelled),true);
		}

	}

	private void showAlert(String message)
	{
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

		alertDialogBuilder.setTitle(getResources().getString(R.string.note));

		alertDialogBuilder
		.setMessage(message)
		.setCancelable(false)
		.setNegativeButton(getResources().getString(R.string.okbuttontext),new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog,int id)
			{
				dialog.dismiss();
			}
		});
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}

	/**
	 * Method for getting Drop off address from latitude and longitude
	 * @param LATITUDE
	 * @param LONGITUDE
	 * @return
	 */
	private String getCompleteAddressString(double LATITUDE, double LONGITUDE) 
	{
		String strAdd = "";
		Geocoder geocoder = new Geocoder(this, Locale.getDefault());
		try {
			List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
			if (addresses != null) 
			{
				Address returnedAddress = addresses.get(0);
				StringBuilder strReturnedAddress = new StringBuilder("");

				for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) 
				{
					strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
				}
				strAdd = strReturnedAddress.toString();
			} 
			else 
			{
			}
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return strAdd;
	}

	/**
	 * Format a time from a given format to given target format
	 * 
	 * @param inputFormat
	 * @param inputTimeStamp
	 * @param outputFormat
	 * @return
	 * @throws ParseException
	 * @throws java.text.ParseException 
	 */
	private static String TimeStampConverter(final String inputFormat,
			String inputTimeStamp, final String outputFormat)
					throws ParseException, java.text.ParseException {
		return new SimpleDateFormat(outputFormat).format(new SimpleDateFormat(
				inputFormat).parse(inputTimeStamp));
	}

	/*private void startTimerToGetFares()
	{
		Utility.printLog("CONTROL INSIDE startTimerToGetDistanceAndEta");

		if(myTimer_publish!= null)
		{
			Utility.printLog("Timer already started");
			return;
		}
		myTimer_publish = new Timer();

		myTimerTask_publish = new TimerTask()
		{
			@Override
			public void run()
			{
				runOnUiThread(new Runnable() 
				{
					public void run()
					{
						try {
							
						double fare = 0.0;
						if(!meter_tax_fare.getText().toString().trim().equals("") && !meter_tax_fare.getText().toString().trim().endsWith(".") && !meter_tax_fare.getText().toString().trim().startsWith("."))
						{
							fare = fare + Double.parseDouble(meter_tax_fare.getText().toString().trim());
						}
						if(!toll_tax_fare.getText().toString().trim().equals("") && !toll_tax_fare.getText().toString().trim().endsWith(".") && !toll_tax_fare.getText().toString().trim().startsWith("."))
						{
							fare = fare + Double.parseDouble(toll_tax_fare.getText().toString().trim());
						}
						if(!parking_tax_fare.getText().toString().trim().equals("") && !parking_tax_fare.getText().toString().trim().endsWith(".") && !parking_tax_fare.getText().toString().trim().startsWith("."))
						{
							fare = fare + Double.parseDouble(parking_tax_fare.getText().toString().trim());
						}
						if(!airport_tax_fare.getText().toString().trim().equals("") && !airport_tax_fare.getText().toString().trim().endsWith(".") && !airport_tax_fare.getText().toString().trim().startsWith("."))
						{
							fare = fare + Double.parseDouble(airport_tax_fare.getText().toString().trim());
						}
						totalFare.setText(""+fare);
						} 
						catch (Exception e) 
						{
							Utility.printLog("startTimerToGetFaresException = "+e);
						}
					}
				});
			}
		};
		myTimer_publish.schedule(myTimerTask_publish, 000, 2000);
	}*/
	/*private Location findLocation(Context cntx,String provider)
	{
		locationManager = (LocationManager) cntx.getSystemService(Service.LOCATION_SERVICE);
		if (locationManager.isProviderEnabled(provider)) 
		{
			locationManager.requestLocationUpdates(provider,
					10, 1000, JourneyDetailsActivity.this);
			if (locationManager != null) 
			{
				location = locationManager.getLastKnownLocation(provider);
				return location;
			}
		}
		return null;
	}

	public double[] getLocation(Context cntx)
	{
		double [] location = new double[2];

		gpsLocation = findLocation(cntx,LocationManager.GPS_PROVIDER);

		Location nwLocation = findLocation(cntx,LocationManager.NETWORK_PROVIDER);

		if (gpsLocation != null) 
		{
			if (nwLocation != null) 
			{
				location[0] = nwLocation.getLatitude();
				location[1] = nwLocation.getLongitude();
				Utility.printLog("data gps"+location[0]+"<---->"+location[1]);
			}

			location[0] = gpsLocation.getLatitude();
			location[1] = gpsLocation.getLongitude();
			Utility.printLog("data network"+location[0]+"<---->"+location[1]);
		} 

		else 
		{
			showSettingsAlert();
		}
		return location;
	}*/
	public void showSettingsAlert()
	{
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

		// Setting Dialog Title
		alertDialog.setTitle("GPS is settings");

		// Setting Dialog Message
		alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

		// Setting Icon to Dialog
		//alertDialog.setIcon(R.drawable.delete);

		// On pressing Settings button
		alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int which) {
				Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
				startActivity(intent);
			}
		});

		// on pressing cancel button
		alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) 
			{
				dialog.cancel();
				finish();
			}
		});

		// Showing Alert Message
		alertDialog.show();
	}
}