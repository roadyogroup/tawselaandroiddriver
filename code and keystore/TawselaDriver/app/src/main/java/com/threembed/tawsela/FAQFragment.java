package com.threembed.tawsela;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import com.app.driverapp.utility.ConnectionDetector;
import com.app.driverapp.utility.SessionManager;
import com.app.driverapp.utility.Utility;
import com.threembed.tawseladriver.R;
import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.PageIndicator;

public class FAQFragment extends Fragment implements android.view.View.OnClickListener
{
	TestFragmentAdapter mAdapter;
	ViewPager mPager;
	PageIndicator mIndicator;
	private android.widget.RelativeLayout supportwebsidelayout;
	private android.widget.TextView haveanissuetextview,privemdtextview;
	@Override
	public void onCreate(android.os.Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
//		HomeFragment.flagForHomeFragmentOpened = false;

	}
	@Override
	public android.view.View onCreateView(android.view.LayoutInflater inflater,	android.view.ViewGroup container,android.os.Bundle savedInstanceState) 
	{
		View view  = inflater.inflate(R.layout.faqfragmentlayout, null);
		initLayoutId(view);
		return view;

	}
	@Override
	public void onResume() 
	{
		super.onResume();
		SessionManager sessionManager = new SessionManager(getActivity());
		sessionManager.setIsFlagForOther(true);
		
	}
	private void initLayoutId(View view)
	{
		mAdapter = new TestFragmentAdapter(getActivity().getSupportFragmentManager());
		mPager = (ViewPager)view.findViewById(R.id.pager);
		mPager.setAdapter(mAdapter);
		mIndicator = (CirclePageIndicator)view.findViewById(R.id.indicator);
		mIndicator.setViewPager(mPager);
		supportwebsidelayout=(android.widget.RelativeLayout)view.findViewById(R.id.supportwebsidelayout);
		Typeface robotoBoldCondensedItalic = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Regular.ttf");
		haveanissuetextview=(android.widget.TextView)view.findViewById(R.id.haveanissuetextview);
		haveanissuetextview.setTypeface(robotoBoldCondensedItalic);
		privemdtextview=(android.widget.TextView)view.findViewById(R.id.privemdtextview);
		supportwebsidelayout.setOnClickListener(this);
	}
	@Override
	public void onClick(android.view.View v) 
	{
		if (v.getId()==R.id.supportwebsidelayout) 
		{

			Utility utility=new Utility();
			ConnectionDetector connectionDetector=new ConnectionDetector(getActivity());
			if (connectionDetector.isConnectingToInternet()) 
			{
				String url = "http://www.tawselaapp.com/";
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(url));
				startActivity(i);
			}
			else 
			{
				//utility.showDialogConfirm(getActivity(),"Alert"," working internet connection required", false).show();
				utility.displayMessageAndExit(getActivity(),"Alert"," working internet connection required");

			}

		}

	}
}
