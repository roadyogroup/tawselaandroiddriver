package com.threembed.tawsela;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.apache.http.NameValuePair;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.driverapp.response.LogoutUser;
import com.app.driverapp.response.ProfileData;
import com.app.driverapp.response.ProfileDetailsData;
import com.app.driverapp.response.ResetPassword;
import com.app.driverapp.utility.ConnectionDetector;
import com.app.driverapp.utility.Scaler;
import com.app.driverapp.utility.SessionManager;
import com.app.driverapp.utility.Utility;
import com.app.driverapp.utility.VariableConstants;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.threembed.tawseladriver.R;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
public class ProfileFragment extends Fragment implements OnClickListener,OnTouchListener,OnFocusChangeListener
{
	//	private Button logoutbutton;
	//private BackGroundTaskForLogout backGroundTaskForLogout;
	private BackGroundTaskFoResetPassword backGroundTaskFoResetPassword;

	private TextView /*numberofreviews,appointmentcompleted,*/avarageratingtextview;
	private EditText driverLicence ,expiryDate, totalEarning,monthEarning,weekEarning, todayEarning,lastBill,totalBooking;

	private android.widget.EditText firstName,lastName,email,mobileNo,/*lincencenotextview,lincenceexpirytextview,*/carMake,carType,seatingCapacity,licensePlate,insuranse;
	private ImageView profile_pic;
	//	private BackGroundTakdFOrGetUserPrfile backGroundTakdFOrGetUserPrfile;
	//private RequestQueue mRequestQueue;
	// private ImageLoader imageLoader ;
	//   private static final String TAG="ProfileFragment";
	private RatingBar ratingBar;
	private String currencySymbol;
	//	private Button passwordreset;
	private MainActivity activityDrower;
	private SessionManager sessionManager;
	//private android.app.ActionBar actionBar;

	@Override
	public void onCreate(android.os.Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		//		HomeFragment.flagForHomeFragmentOpened = false;

		Utility utility=new Utility();
		ConnectionDetector connectionDetector=new ConnectionDetector(getActivity());
		sessionManager = new SessionManager(getActivity());
		sessionManager.setIsFlagForOther(true);
		Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
		List<Address> addresses;
		try 
		{
			addresses = geocoder.getFromLocation(sessionManager.getDriverCurrentLat(), sessionManager.getDriverCurrentLongi(), 1);
			Address obj = addresses.get(0);
			currencySymbol = Currency.getInstance(obj.getLocale()).getSymbol();
		} catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		if (connectionDetector.isConnectingToInternet()) 
		{
			getUserProfile();

		}
		else 
		{
			//utility.showDialogConfirm(getActivity(),"Alert"," working internet connection required", false).show();
			utility.displayMessageAndExit(getActivity(),getResources().getString(R.string.Pleasewaitmessage),getResources().getString(R.string.internetconnectionmessage));

		}


		activityDrower=(MainActivity)getActivity();

		TextView titleTextview = activityDrower.getTitleTextview();
		titleTextview.setText(getResources().getString(R.string.myprofile));
	}
	private void getUserProfile()
	{ 
		SessionManager sessionManager=new SessionManager(getActivity());
		Utility utility=new Utility();
		String sessionToken=sessionManager.getSessionToken();
		String deviceid=Utility.getDeviceId(getActivity());
		String currentDate=utility.getCurrentGmtTime();
		final String mparams[]={sessionToken,deviceid,currentDate};
		final ProgressDialog mdialog;
		mdialog=Utility.GetProcessDialog(getActivity());
		mdialog.setMessage(getResources().getString(R.string.Pleasewaitmessage));
		mdialog.show();
		mdialog.setCancelable(false);
		RequestQueue queue = Volley.newRequestQueue(getActivity());  // this = context
		String	url = VariableConstants.getProfileinfo_url;
		StringRequest postRequest = new StringRequest(Request.Method.POST, url,
				new Response.Listener<String>()
				{
			@Override
			public void onResponse(String response) 
			{
				// response
				Log.d("ProfileResponse", response);
				ProfileData  profileData;
				Gson gson = new Gson();

				profileData=gson.fromJson(response, ProfileData.class);
				Utility.printLog("Profileresponse"+response);

				if (mdialog!=null)
				{
					mdialog.dismiss();
					mdialog.cancel();
					//mdialog=null;
				}
				try
				{						
					if (profileData.getErrFlag()==0&&profileData.getErrNum()==21)
					{
						ProfileDetailsData profileDetailsData  = profileData.getData();

						// 21 -> (0) Got the details!
						Utility utility=new Utility();
						String imagehosturl=utility.getImageHostUrl(getActivity());
						String Imageurl=imagehosturl+profileDetailsData.getpPic();
						String firstNamestr=profileDetailsData.getfName();
						String lastNamestr=profileDetailsData.getlName();
						String mobilenostr=profileDetailsData.getMobile();
						String emailaddressstr=profileDetailsData.getEmail();
						String licPlateNumstr = profileDetailsData.getLicPlateNum();
						String carTypestr= profileDetailsData.getVehicleType();
						String carMakestr = profileDetailsData.getVehMake();
						String seatingCapacitystr = profileDetailsData.getSeatCapacity();
						String insuransestr = profileDetailsData.getVehicleInsuranceNum();

						String driverLicstr=profileDetailsData.getLicNo();
						String expirydatestr = profileDetailsData.getLicExp();
						String totalEarningstr = currencySymbol+profileDetailsData.getTotalAmt();
						String monthEarstr = currencySymbol +profileDetailsData.getMonthAmt();
						String weekEarstr = currencySymbol +profileDetailsData.getWeekAmt();
						String todayEar = currencySymbol +profileDetailsData.getTodayAmt();
						String lastBillstr = currencySymbol +profileDetailsData.getLastBilledAmt();
						String totalBookingstr = profileDetailsData.getTotRats();

						double arrayOfscallingfactor[]=Scaler.getScalingFactorwithDpConvesion(getActivity());
						int topstriplayoutmarigin = 300; // margin in dips
						int topstripMargin = (int)Math.round(topstriplayoutmarigin*arrayOfscallingfactor[1]);//(int)(topstriplayoutmarigin * d); // margin in pixels
						Picasso.with(getActivity()) //
						.load(Imageurl) //
						.placeholder(R.drawable.default_user) //
						.error(R.drawable.default_user)/*.fit()*/
						.resize(topstripMargin,topstripMargin)	 //
						.into(profile_pic);

						firstName.setText(""+firstNamestr);
						lastName.setText(""+lastNamestr);
						mobileNo.setText(getResources().getString(R.string.mobilenopro)+mobilenostr);
						email.setText(getResources().getString(R.string.emailpro)+emailaddressstr);
						carMake.setText(getResources().getString(R.string.carmake)+carMakestr);
						carType.setText(getResources().getString(R.string.cartype)+carTypestr);
						licensePlate.setText(getResources().getString(R.string.licenseplate)+licPlateNumstr);
						seatingCapacity.setText(getResources().getString(R.string.seatingcapacity)+seatingCapacitystr);
						insuranse.setText(getResources().getString(R.string.insuranse)+insuransestr); 
						driverLicence.setText(getResources().getString(R.string.driverlicence)+" "+driverLicstr);
						expiryDate.setText(getResources().getString(R.string.expiredate)+" "+expirydatestr);
						totalEarning.setText(getResources().getString(R.string.total_earning)+" "+totalEarningstr);
						monthEarning.setText(getResources().getString(R.string.monthearning)+" "+monthEarstr);
						weekEarning.setText(getResources().getString(R.string.weekearning)+" "+weekEarstr);
						todayEarning.setText(getResources().getString(R.string.todayearning)+" "+todayEar);
						lastBill.setText(getResources().getString(R.string.lastbill)+" "+lastBillstr);
						totalBooking.setText(getResources().getString(R.string.totalbooking)+" "+totalBookingstr);
						avarageratingtextview.setText("   "+getResources().getString(R.string.rating)+"  :"+profileDetailsData.getAvgRate());
						ratingBar.setRating(profileDetailsData.getAvgRate()); 

					}
					else if (profileData.getErrFlag()==1 && profileData.getErrNum()==3)
					{

						// 3 -> (1) Error occurred while processing your request.
						ErrorMessage(getResources().getString(R.string.messagetitle),profileData.getErrMsg(),true);
					}
					else if (profileData.getErrFlag()==1 && profileData.getErrNum()==7)
					{
						//7 -> (1) Invalid token, please login or register.
						ErrorMessageForInvalidOrExpired(getResources().getString(R.string.messagetitle),profileData.getErrMsg());
					}
					else if (profileData.getErrFlag() == 1 && profileData.getErrNum()==6)
					{
						//6-> (1)  Session token expired, please login.
						ErrorMessageForInvalidOrExpired(getResources().getString(R.string.messagetitle),profileData.getErrMsg());
					}
					else if (profileData.getErrFlag() == 1 && profileData.getErrNum()==101)
					{
						//6-> (1)  Session token expired, please login.
						ErrorMessageForInvalidOrExpired(getResources().getString(R.string.messagetitle),profileData.getErrMsg());
					}
					else if (profileData.getErrFlag() == 1 && profileData.getErrNum()==1)
					{
						//1 -> (1) Mandatory field missing
						ErrorMessage(getResources().getString(R.string.messagetitle),profileData.getErrMsg(),true);
					}
				} 
				catch (Exception e) 
				{
					ErrorMessage(getResources().getString(R.string.messagetitle),getResources().getString(R.string.servererror),true);
				}

			}
				},
				new Response.ErrorListener()
				{
					@Override
					public void onErrorResponse(VolleyError error) 
					{
						if (mdialog!=null)
						{
							mdialog.dismiss();
							mdialog.cancel();
							//mdialog=null;
						}
						//    ErrorMessage(getResources().getString(R.string.messagetitle),getResources().getString(R.string.servererror),true);
					}
				}
				) {    
			@Override
			protected Map<String, String> getParams()
			{ 
				Map<String, String>  params = new HashMap<String, String>(); 
				params.put("ent_sess_token", mparams[0]); 
				params.put("ent_dev_id", mparams[1]);
				params.put("ent_date_time", mparams[2]);

				return params; 
			}
		};
		int socketTimeout = 60000;//60 seconds - change to what you want
		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		postRequest.setRetryPolicy(policy);
		queue.add(postRequest);


	}

	private void ErrorMessage(String title,String message,final boolean flageforSwithchActivity)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(title);
		builder.setMessage(message);

		builder.setPositiveButton(getResources().getString(R.string.okbuttontext),
				new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				if (flageforSwithchActivity) 
				{

				}
				else
				{
					// only show message  
				}
				dialog.dismiss();
			}
		});

		AlertDialog	 alert = builder.create();
		alert.setCancelable(false);
		alert.show();
	}

	private void ErrorMessageForInvalidOrExpired(String title,String message)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(title);
		builder.setMessage(message);

		builder.setPositiveButton(getResources().getString(R.string.cancelbutton),
				new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
			}
		});

		builder.setNegativeButton(getResources().getString(R.string.okbuttontext),
				new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
				Intent intent=new Intent(getActivity(), SplahsActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
				SessionManager sessionManager=new SessionManager(getActivity());
				sessionManager.logoutUser();
				startActivity(intent);
				getActivity().finish();

			}
		});
		AlertDialog	 alert = builder.create();
		alert.setCancelable(false);
		alert.show();
	}

	@Override
	public android.view.View onCreateView(android.view.LayoutInflater inflater,android.view.ViewGroup container,android.os.Bundle savedInstanceState) 
	{
		View view  = inflater.inflate(R.layout.profilelayout, null);
		initlayoutid(view);
		return view;
	}

	private void initlayoutid(View view)
	{
		profile_pic=(ImageView)view.findViewById(R.id.su_one_profile_image);


		firstName=(android.widget.EditText)view.findViewById(R.id.su_one_first_name_et);
		lastName=(android.widget.EditText)view.findViewById(R.id.su_one_last_name_et);
		mobileNo=(android.widget.EditText)view.findViewById(R.id.su_one_mobile_no);

		email=(android.widget.EditText)view.findViewById(R.id.su_one_email_et);
		driverLicence=(android.widget.EditText)view.findViewById(R.id.su_one_driver_lic);
		expiryDate=(android.widget.EditText)view.findViewById(R.id.su_one_expiry_date);

		carMake=(android.widget.EditText)view.findViewById(R.id.car_make);
		carType=(android.widget.EditText)view.findViewById(R.id.car_type);
		seatingCapacity=(android.widget.EditText)view.findViewById(R.id.seating_capacity);
		licensePlate=(android.widget.EditText)view.findViewById(R.id.license_plate);
		insuranse=(android.widget.EditText)view.findViewById(R.id.insurance);

		totalEarning=(android.widget.EditText)view.findViewById(R.id.totalearning);
		monthEarning=(android.widget.EditText)view.findViewById(R.id.monthearning);
		weekEarning=(android.widget.EditText)view.findViewById(R.id.weekearning);
		todayEarning=(android.widget.EditText)view.findViewById(R.id.todayearning);
		lastBill=(android.widget.EditText)view.findViewById(R.id.lastbill);
		totalBooking=(android.widget.EditText)view.findViewById(R.id.totalbooking);

		ratingBar = (RatingBar)view.findViewById(R.id.ratingBar);
		
		ratingBar.setEnabled(false);
		//		 lincencenotextview=(android.widget.EditText)view.findViewById(R.id.lincencenotextview);
		//		 lincenceexpirytextview=(android.widget.EditText)view.findViewById(R.id.lincenceexpirytextview);
		//		 numberofreviews=(TextView)view.findViewById(R.id.numberofreviews);
		//		 appointmentcompleted=(TextView)view.findViewById(R.id.appointmentcompleted);
		//		 passwordreset=(Button)view.findViewById(R.id.passwordreset);
		avarageratingtextview=(TextView)view.findViewById(R.id.avarageratingtextview);

		firstName.setCursorVisible(false);
		lastName.setCursorVisible(false);
		mobileNo.setCursorVisible(false);
		email.setCursorVisible(false);
		driverLicence.setCursorVisible(false);
		expiryDate.setCursorVisible(false);

		//		 lincencenotextview.setCursorVisible(false);
		//		 lincenceexpirytextview.setCursorVisible(false);
		carMake.setCursorVisible(false);
		carType.setCursorVisible(false);
		seatingCapacity.setCursorVisible(false);
		licensePlate.setCursorVisible(false);
		insuranse.setCursorVisible(false);

		totalEarning.setCursorVisible(false);
		monthEarning.setCursorVisible(false);
		weekEarning.setCursorVisible(false);
		todayEarning.setCursorVisible(false);
		lastBill.setCursorVisible(false);
		totalBooking.setCursorVisible(false);

		firstName.clearFocus();
		lastName.clearFocus();
		mobileNo.clearFocus();
		email.clearFocus();

		driverLicence.clearFocus();
		expiryDate.clearFocus();
		totalEarning.clearFocus();
		monthEarning.clearFocus();
		weekEarning.clearFocus();
		todayEarning.clearFocus();
		lastBill.clearFocus();
		totalBooking.clearFocus();
		//		 lincencenotextview.clearFocus();
		//		 lincenceexpirytextview.clearFocus();
		carMake.clearFocus();
		carType.clearFocus();
		seatingCapacity.clearFocus();
		licensePlate.clearFocus();
		insuranse.clearFocus();



		firstName.setOnTouchListener(this);
		lastName.setOnTouchListener(this);
		mobileNo.setOnTouchListener(this);
		email.setOnTouchListener(this);

		driverLicence.setOnTouchListener(this);
		expiryDate.setOnTouchListener(this);
		totalEarning.setOnTouchListener(this);
		monthEarning.setOnTouchListener(this);
		weekEarning.setOnTouchListener(this);
		todayEarning.setOnTouchListener(this);
		lastBill.setOnTouchListener(this);
		totalBooking.setOnTouchListener(this);
		//		 lincencenotextview.setOnTouchListener(this);
		//		 lincenceexpirytextview.setOnTouchListener(this);
		carMake.setOnTouchListener(this);
		carType.setOnTouchListener(this);
		seatingCapacity.setOnTouchListener(this);
		licensePlate.setOnTouchListener(this);
		insuranse.setOnTouchListener(this);


		firstName.setOnFocusChangeListener(this);
		lastName.setOnFocusChangeListener(this);
		mobileNo.setOnFocusChangeListener(this);
		email.setOnFocusChangeListener(this);

		driverLicence.setOnFocusChangeListener(this);
		expiryDate.setOnFocusChangeListener(this);
		totalEarning.setOnFocusChangeListener(this);
		monthEarning.setOnFocusChangeListener(this);
		weekEarning.setOnFocusChangeListener(this);
		todayEarning.setOnFocusChangeListener(this);
		lastBill.setOnFocusChangeListener(this);
		totalBooking.setOnFocusChangeListener(this);
		//		 lincencenotextview.setOnFocusChangeListener(this);
		//		 lincenceexpirytextview.setOnFocusChangeListener(this);
		carMake.setOnFocusChangeListener(this);
		carType.setOnFocusChangeListener(this);
		seatingCapacity.setOnFocusChangeListener(this);
		licensePlate.setOnFocusChangeListener(this);
		insuranse.setOnFocusChangeListener(this);

		int alpha =204;
		Typeface robotoBoldCondensedItalic = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Zurich Light Condensed.ttf");

		firstName.setTypeface(robotoBoldCondensedItalic);
		firstName.setTextColor(Color.argb(alpha, 51, 51, 51));

		lastName.setTypeface(robotoBoldCondensedItalic);
		lastName.setTextColor(Color.argb(alpha, 51, 51, 51));

		mobileNo.setTypeface(robotoBoldCondensedItalic);
		mobileNo.setTextColor(Color.argb(alpha, 51, 51, 51));


		email.setTypeface(robotoBoldCondensedItalic);
		email.setTextColor(Color.argb(alpha, 51, 51, 51));

		driverLicence.setTypeface(robotoBoldCondensedItalic);
		driverLicence.setTextColor(Color.argb(alpha, 51, 51, 51));

		expiryDate.setTypeface(robotoBoldCondensedItalic);
		expiryDate.setTextColor(Color.argb(alpha, 51, 51, 51));

		totalEarning.setTypeface(robotoBoldCondensedItalic);
		totalEarning.setTextColor(Color.argb(alpha, 51, 51, 51));


		monthEarning.setTypeface(robotoBoldCondensedItalic);
		monthEarning.setTextColor(Color.argb(alpha, 51, 51, 51));

		weekEarning.setTypeface(robotoBoldCondensedItalic);
		weekEarning.setTextColor(Color.argb(alpha, 51, 51, 51));

		todayEarning.setTypeface(robotoBoldCondensedItalic);
		todayEarning.setTextColor(Color.argb(alpha, 51, 51, 51));

		lastBill.setTypeface(robotoBoldCondensedItalic);
		lastBill.setTextColor(Color.argb(alpha, 51, 51, 51));


		totalBooking.setTypeface(robotoBoldCondensedItalic);
		totalBooking.setTextColor(Color.argb(alpha, 51, 51, 51));

		//		 lincencenotextview.setTypeface(robotoBoldCondensedItalic);
		//		 lincencenotextview.setTextColor(Color.argb(alpha, 51, 51, 51));
		//		 
		//		 lincenceexpirytextview.setTypeface(robotoBoldCondensedItalic);
		//		 lincenceexpirytextview.setTextColor(Color.argb(alpha, 51, 51, 51));

		carMake.setTypeface(robotoBoldCondensedItalic);
		carMake.setTextColor(Color.argb(alpha, 51, 51, 51));

		carType.setTypeface(robotoBoldCondensedItalic);
		carType.setTextColor(Color.argb(alpha, 51, 51, 51));

		seatingCapacity.setTypeface(robotoBoldCondensedItalic);
		seatingCapacity.setTextColor(Color.argb(alpha, 51, 51, 51));

		licensePlate.setTypeface(robotoBoldCondensedItalic);
		licensePlate.setTextColor(Color.argb(alpha, 51, 51, 51));

		insuranse.setTypeface(robotoBoldCondensedItalic);
		insuranse.setTextColor(Color.argb(alpha, 51, 51, 51));


		//		 numberofreviews.setTypeface(robotoBoldCondensedItalic);
		//		 numberofreviews.setTextColor(Color.argb(alpha, 51, 51, 51));


		avarageratingtextview.setTypeface(robotoBoldCondensedItalic);
		avarageratingtextview.setTextColor(Color.argb(alpha, 51, 51, 51));

		//		 appointmentcompleted.setTypeface(robotoBoldCondensedItalic);
		//		 appointmentcompleted.setTextColor(Color.argb(alpha, 51, 51, 51));
		/*		 firstName.setCursorVisible(false);
		 firstName.setCursorVisible(false);

		 Typeface buttontypeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Zurich Condensed.ttf");
		 logoutbutton.setTextColor(Color.argb(204, 252, 252, 252));
		 passwordreset.setTextColor(Color.argb(204, 252, 252, 252));

		 passwordreset.setTypeface(buttontypeface);
		 logoutbutton.setTypeface(buttontypeface);
		 */
	}

	@Override
	public void onClick(View v) 
	{
		/*if (v.getId()==R.id.logoutbutton)
		{
			ErrorMessageForLogout(getResources().getString(R.string.logout),getResources().getString(R.string.logoutmessage));
		}
		if (v.getId()==R.id.passwordreset) 
		{
			ErrorMessageForPasswordChange(getResources().getString(R.string.resetpassword),getResources().getString(R.string.resetpasswordmessage));
		}*/

	}
	@Override
	public boolean onTouch(android.view.View v, android.view.MotionEvent event)
	{
		if (v.getId()==R.id.su_one_first_name_et) 
		{
			firstName.setCursorVisible(false);
			firstName.setFocusable(false);
			if (event.getAction()==MotionEvent.ACTION_DOWN) 
			{
				//selectOptionMenu();
			}
			int inType = firstName.getInputType(); // backup the input type
			firstName.setInputType(InputType.TYPE_NULL); // disable soft input
			firstName.onTouchEvent(event); // call native handler
			firstName.setInputType(inType); // restore input type

			return true; // consume touch even
		}
		if (v.getId()==R.id.su_one_last_name_et) 
		{
			lastName.setCursorVisible(false);
			lastName.setFocusable(false);
			if (event.getAction()==MotionEvent.ACTION_DOWN) 
			{
				//selectOptionMenu();
			}
			int inType = lastName.getInputType(); // backup the input type
			lastName.setInputType(InputType.TYPE_NULL); // disable soft input
			lastName.onTouchEvent(event); // call native handler
			lastName.setInputType(inType); // restore input type

			return true; // consume touch even
		}
		if (v.getId()==R.id.su_one_mobile_no) 
		{
			mobileNo.setCursorVisible(false);
			mobileNo.setFocusable(false);
			if (event.getAction()==MotionEvent.ACTION_DOWN) 
			{
				//selectOptionMenu();
			}
			int inType = mobileNo.getInputType(); // backup the input type
			mobileNo.setInputType(InputType.TYPE_NULL); // disable soft input
			mobileNo.onTouchEvent(event); // call native handler
			mobileNo.setInputType(inType); // restore input type

			return true; // consume touch even
		}
		if (v.getId()==R.id.su_one_email_et) 
		{
			email.setCursorVisible(false);
			email.setFocusable(false);
			if (event.getAction()==MotionEvent.ACTION_DOWN) 
			{
				//selectOptionMenu();
			}
			int inType = email.getInputType(); // backup the input type
			email.setInputType(InputType.TYPE_NULL); // disable soft input
			email.onTouchEvent(event); // call native handler
			email.setInputType(inType); // restore input type

			return true; // consume touch even
		}
		if (v.getId()==R.id.su_one_driver_lic) 
		{
			driverLicence.setCursorVisible(false);
			driverLicence.setFocusable(false);
			if (event.getAction()==MotionEvent.ACTION_DOWN) 
			{
				//selectOptionMenu();
			}
			int inType = driverLicence.getInputType(); // backup the input type
			driverLicence.setInputType(InputType.TYPE_NULL); // disable soft input
			driverLicence.onTouchEvent(event); // call native handler
			driverLicence.setInputType(inType); // restore input type

			return true; // consume touch even
		}
		if (v.getId()==R.id.su_one_expiry_date) 
		{
			expiryDate.setCursorVisible(false);
			expiryDate.setFocusable(false);
			if (event.getAction()==MotionEvent.ACTION_DOWN) 
			{
				//selectOptionMenu();
			}
			int inType = expiryDate.getInputType(); // backup the input type
			expiryDate.setInputType(InputType.TYPE_NULL); // disable soft input
			expiryDate.onTouchEvent(event); // call native handler
			expiryDate.setInputType(inType); // restore input type

			return true; // consume touch even
		}
		if (v.getId()==R.id.totalearning) 
		{
			totalEarning.setCursorVisible(false);
			totalEarning.setFocusable(false);
			if (event.getAction()==MotionEvent.ACTION_DOWN) 
			{
				//selectOptionMenu();
			}
			int inType = totalEarning.getInputType(); // backup the input type
			totalEarning.setInputType(InputType.TYPE_NULL); // disable soft input
			totalEarning.onTouchEvent(event); // call native handler
			totalEarning.setInputType(inType); // restore input type

			return true; // consume touch even
		}
		if (v.getId()==R.id.monthearning) 
		{
			monthEarning.setCursorVisible(false);
			monthEarning.setFocusable(false);
			if (event.getAction()==MotionEvent.ACTION_DOWN) 
			{
				//selectOptionMenu();
			}
			int inType = monthEarning.getInputType(); // backup the input type
			monthEarning.setInputType(InputType.TYPE_NULL); // disable soft input
			monthEarning.onTouchEvent(event); // call native handler
			monthEarning.setInputType(inType); // restore input type

			return true; // consume touch even
		}
		if (v.getId()==R.id.weekearning) 
		{
			weekEarning.setCursorVisible(false);
			weekEarning.setFocusable(false);
			if (event.getAction()==MotionEvent.ACTION_DOWN) 
			{
				//selectOptionMenu();
			}
			int inType = weekEarning.getInputType(); // backup the input type
			weekEarning.setInputType(InputType.TYPE_NULL); // disable soft input
			weekEarning.onTouchEvent(event); // call native handler
			weekEarning.setInputType(inType); // restore input type

			return true; // consume touch even
		}
		if (v.getId()==R.id.todayearning) 
		{
			todayEarning.setCursorVisible(false);
			todayEarning.setFocusable(false);
			if (event.getAction()==MotionEvent.ACTION_DOWN) 
			{
				//selectOptionMenu();
			}
			int inType = todayEarning.getInputType(); // backup the input type
			todayEarning.setInputType(InputType.TYPE_NULL); // disable soft input
			todayEarning.onTouchEvent(event); // call native handler
			todayEarning.setInputType(inType); // restore input type

			return true; // consume touch even
		}
		if (v.getId()==R.id.lastbill) 
		{
			lastBill.setCursorVisible(false);
			lastBill.setFocusable(false);
			if (event.getAction()==MotionEvent.ACTION_DOWN) 
			{
				//selectOptionMenu();
			}
			int inType = lastBill.getInputType(); // backup the input type
			lastBill.setInputType(InputType.TYPE_NULL); // disable soft input
			lastBill.onTouchEvent(event); // call native handler
			lastBill.setInputType(inType); // restore input type

			return true; // consume touch even
		}
		if (v.getId()==R.id.totalbooking) 
		{
			totalBooking.setCursorVisible(false);
			totalBooking.setFocusable(false);
			if (event.getAction()==MotionEvent.ACTION_DOWN) 
			{
				//selectOptionMenu();
			}
			int inType = totalBooking.getInputType(); // backup the input type
			totalBooking.setInputType(InputType.TYPE_NULL); // disable soft input
			totalBooking.onTouchEvent(event); // call native handler
			totalBooking.setInputType(inType); // restore input type

			return true; // consume touch even
		}

		if (v.getId()==R.id.car_make) 
		{
			carMake.setCursorVisible(false);
			carMake.setFocusable(false);
			if (event.getAction()==MotionEvent.ACTION_DOWN) 
			{
				//selectOptionMenu();
			}
			int inType = email.getInputType(); // backup the input type
			carMake.setInputType(InputType.TYPE_NULL); // disable soft input
			carMake.onTouchEvent(event); // call native handler
			carMake.setInputType(inType); // restore input type

			return true; // consume touch even
		}
		if (v.getId()==R.id.car_type) 
		{
			carType.setCursorVisible(false);
			carType.setFocusable(false);
			if (event.getAction()==MotionEvent.ACTION_DOWN) 
			{
				//selectOptionMenu();
			}
			int inType = email.getInputType(); // backup the input type
			carType.setInputType(InputType.TYPE_NULL); // disable soft input
			carType.onTouchEvent(event); // call native handler
			carType.setInputType(inType); // restore input type

			return true; // consume touch even
		}
		if (v.getId()==R.id.seating_capacity) 
		{
			seatingCapacity.setCursorVisible(false);
			seatingCapacity.setFocusable(false);
			if (event.getAction()==MotionEvent.ACTION_DOWN) 
			{
				//selectOptionMenu();
			}
			int inType = email.getInputType(); // backup the input type
			seatingCapacity.setInputType(InputType.TYPE_NULL); // disable soft input
			seatingCapacity.onTouchEvent(event); // call native handler
			seatingCapacity.setInputType(inType); // restore input type

			return true; // consume touch even
		}
		if (v.getId()==R.id.license_plate) 
		{
			licensePlate.setCursorVisible(false);
			licensePlate.setFocusable(false);
			if (event.getAction()==MotionEvent.ACTION_DOWN) 
			{
				//selectOptionMenu();
			}
			int inType = email.getInputType(); // backup the input type
			licensePlate.setInputType(InputType.TYPE_NULL); // disable soft input
			licensePlate.onTouchEvent(event); // call native handler
			licensePlate.setInputType(inType); // restore input type

			return true; // consume touch even
		}
		if (v.getId()==R.id.insurance) 
		{
			insuranse.setCursorVisible(false);
			insuranse.setFocusable(false);
			if (event.getAction()==MotionEvent.ACTION_DOWN) 
			{
				//selectOptionMenu();
			}
			int inType = email.getInputType(); // backup the input type
			insuranse.setInputType(InputType.TYPE_NULL); // disable soft input
			insuranse.onTouchEvent(event); // call native handler
			insuranse.setInputType(inType); // restore input type

			return true; // consume touch even
		}
		/*		if (v.getId()==R.id.lincencenotextview) 
		{
			lincencenotextview.setCursorVisible(false);
			lincencenotextview.setFocusable(false);
			if (event.getAction()==MotionEvent.ACTION_DOWN) 
			{
				//selectOptionMenu();
			}
			int inType = lincencenotextview.getInputType(); // backup the input type
			lincencenotextview.setInputType(InputType.TYPE_NULL); // disable soft input
			lincencenotextview.onTouchEvent(event); // call native handler
			lincencenotextview.setInputType(inType); // restore input type

			return true; // consume touch even
		}
		if (v.getId()==R.id.lincenceexpirytextview) 
		{
			lincenceexpirytextview.setCursorVisible(false);
			lincenceexpirytextview.setFocusable(false);
			if (event.getAction()==MotionEvent.ACTION_DOWN) 
			{
				//selectOptionMenu();
			}
			int inType = lincenceexpirytextview.getInputType(); // backup the input type
			lincenceexpirytextview.setInputType(InputType.TYPE_NULL); // disable soft input
			lincenceexpirytextview.onTouchEvent(event); // call native handler
			lincenceexpirytextview.setInputType(inType); // restore input type

			return true; // consume touch even
		}*/

		else 
		{
			return true;
		}

	}

	@Override
	public void onFocusChange(View v, boolean hasFocus)
	{
		if (v.getId()==R.id.su_one_first_name_et) 
		{
			firstName.setCursorVisible(false);
			firstName.setFocusable(false);

		}
		if (v.getId()==R.id.su_one_last_name_et) 
		{
			lastName.setCursorVisible(false);
			lastName.setFocusable(false);
		}
		if (v.getId()==R.id.su_one_mobile_no) 
		{
			mobileNo.setCursorVisible(false);
			mobileNo.setFocusable(false);
		}
		if (v.getId()==R.id.su_one_email_et) 
		{
			email.setCursorVisible(false);
			email.setFocusable(false);

		}

		if (v.getId()==R.id.su_one_driver_lic) 
		{
			driverLicence.setCursorVisible(false);
			driverLicence.setFocusable(false);

		}
		if (v.getId()==R.id.su_one_expiry_date) 
		{
			expiryDate.setCursorVisible(false);
			expiryDate.setFocusable(false);
		}
		if (v.getId()==R.id.totalearning) 
		{
			totalEarning.setCursorVisible(false);
			totalEarning.setFocusable(false);
		}
		if (v.getId()==R.id.monthearning) 
		{
			monthEarning.setCursorVisible(false);
			monthEarning.setFocusable(false);

		}

		if (v.getId()==R.id.weekearning) 
		{
			weekEarning.setCursorVisible(false);
			weekEarning.setFocusable(false);

		}
		if (v.getId()==R.id.todayearning) 
		{
			todayEarning.setCursorVisible(false);
			todayEarning.setFocusable(false);
		}
		if (v.getId()==R.id.lastbill) 
		{
			lastBill.setCursorVisible(false);
			lastBill.setFocusable(false);
		}
		if (v.getId()==R.id.totalbooking) 
		{
			totalBooking.setCursorVisible(false);
			totalBooking.setFocusable(false);

		}
		//		if (v.getId()==R.id.lincencenotextview) 
		//		{
		//			 lincencenotextview.setCursorVisible(false);
		//			 lincencenotextview.setFocusable(false);
		//			
		//		}
		//		if (v.getId()==R.id.lincenceexpirytextview) 
		//		{
		//			 lincenceexpirytextview.setCursorVisible(false);
		//			 lincenceexpirytextview.setFocusable(false);
		//		}

		if (v.getId()==R.id.car_make) 
		{
			carMake.setCursorVisible(false);
			carMake.setFocusable(false);

		}

		if (v.getId()==R.id.car_type) 
		{
			carType.setCursorVisible(false);
			carType.setFocusable(false);

		}

		if (v.getId()==R.id.seating_capacity) 
		{
			seatingCapacity.setCursorVisible(false);
			seatingCapacity.setFocusable(false);

		}

		if (v.getId()==R.id.license_plate) 
		{
			licensePlate.setCursorVisible(false);
			licensePlate.setFocusable(false);

		}

		if (v.getId()==R.id.insurance) 
		{
			insuranse.setCursorVisible(false);
			insuranse.setFocusable(false);

		}

		else 
		{

		}

	}



	/*private void ErrorMessageForLogout(String title,String message)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(title);
		builder.setMessage(message);



		builder.setNegativeButton(getResources().getString(R.string.logout),
				new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{

				dialog.dismiss();
				logoutUser();;
			}
		});

		builder.setPositiveButton(getResources().getString(R.string.cancle),
				new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{

				dialog.dismiss();
			}
		});
		AlertDialog	 alert = builder.create();
		alert.setCancelable(false);
		alert.show();
	}


	private void ErrorMessageForPasswordChange(String title,String message)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(title);
		builder.setMessage(message);



		builder.setNegativeButton(getResources().getString(R.string.okbuttontext),
				new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{

				dialog.dismiss();
				resetPassword();
			}
		});

		builder.setPositiveButton(getResources().getString(R.string.cancle),
				new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{

				dialog.dismiss();
			}
		});
		AlertDialog	 alert = builder.create();
		alert.setCancelable(false);
		alert.show();
	}*/



	/*private void logoutUser()
	{
		SessionManager sessionManager=new SessionManager(getActivity());
		//Utility utility=new Utility();
		String deviceid=Utility.getDeviceId(getActivity());
		Utility utility=new Utility();
		String sessionToken=sessionManager.getSessionToken();
		String logoutType=VariableConstants.USERTYPE;
		  String curenttime=utility.getCurrentGmtTime();
		String params[]={sessionToken,deviceid,logoutType,curenttime};
		backGroundTaskForLogout=new BackGroundTaskForLogout();
		backGroundTaskForLogout.execute(params);

	}*/

	private void resetPassword()
	{
		SessionManager sessionManager=new SessionManager(getActivity());
		//Utility utility=new Utility();
		String deviceid=Utility.getDeviceId(getActivity());
		Utility utility=new Utility();
		String sessionToken=sessionManager.getSessionToken();
		//String logoutType=VariableConstants.LOGOUTTYPE;
		String curenttime=utility.getCurrentGmtTime();
		String params[]={sessionToken,deviceid,curenttime};
		backGroundTaskFoResetPassword=new BackGroundTaskFoResetPassword();
		backGroundTaskFoResetPassword.execute(params);

	}

	private class BackGroundTaskForLogout extends android.os.AsyncTask<String, Void, LogoutUser>
	{
		private Utility utility=new Utility();
		private List<NameValuePair>logoutParamList;
		private String logoutResponse;
		private LogoutUser logoutUser;
		private boolean isSuccess=true;
		private  ProgressDialog mdialog;

		@Override
		protected LogoutUser doInBackground(String... params) 
		{
			logoutParamList=utility.getLogoutParameter(params);
			logoutResponse=  utility.makeHttpRequest(VariableConstants.logOut_url,VariableConstants.methodeName,logoutParamList);
			if (logoutResponse!=null)
			{
				Gson gson = new Gson();
				logoutUser=gson.fromJson(logoutResponse, LogoutUser.class);
			}
			else 
			{
				isSuccess=false;
			}
			return logoutUser;
		}
		@Override
		protected void onPreExecute()
		{
			super.onPreExecute();
			mdialog=Utility.GetProcessDialog(getActivity());
			mdialog.setMessage(getResources().getString(R.string.Pleasewaitmessage));
			mdialog.show();
		}
		@Override
		protected void onPostExecute(LogoutUser result)
		{
			super.onPostExecute(result);
			if (mdialog!=null)
			{
				mdialog.dismiss();
				mdialog.cancel();
				mdialog=null;
			}
			//	Error Codes:
			//ErrorNumber -> (ErrorFlag) ErrorMessage
			if (isSuccess) 
			{
				if (result.getErrFlag()==0&&result.getErrNum()==29) 
				{
					///29 -> (0) Logged out!
					startLogin();

				}
				else if (result.getErrFlag()==1&&result.getErrNum()==1) 
				{
					//1 -> (1) Mandatory field missing
					ErrorMessage(getResources().getString(R.string.error),result.getErrMsg(),false);

				}
				else if (result.getErrFlag()==1&&result.getErrNum()==6) 
				{
					//6 -> (1) Session token expired, please login.
					ErrorMessageForSessionExpoired(getResources().getString(R.string.error),result.getErrMsg(),result.getErrFlag(),result.getErrNum());

				}
				else if (result.getErrFlag()==1&&result.getErrNum()==7) 
				{
					//7 -> (1) Invalid token, please login or register.
					ErrorMessageForSessionExpoired(getResources().getString(R.string.error),result.getErrMsg(),result.getErrFlag(),result.getErrNum());

				}
				else if (result.getErrFlag()==1&&result.getErrNum()==3) 
				{
					//3 -> (1) Error occurred while processing your request.
					ErrorMessage(getResources().getString(R.string.error),result.getErrMsg(),false);
				}
			}
			else 
			{
				// some serviere issuse accured
				ErrorMessage(getResources().getString(R.string.error),"Request timeout",false);

			}


		}
		@Override
		protected void onCancelled()
		{
			super.onCancelled();
		}
		@Override
		protected void onCancelled(LogoutUser result) 
		{
			super.onCancelled(result);
		}



		private void startLogin()
		{
			SessionManager sessionManager=new SessionManager(getActivity());
			sessionManager.logoutUser();
			Intent intent=new Intent(getActivity(), SplahsActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			getActivity().finish();
		}


		private void ErrorMessageForSessionExpoired(String title,final String message,final int errorFlag,final int errornum)
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle(title);
			builder.setMessage(message);

			builder.setPositiveButton(getResources().getString(R.string.okbuttontext),
					new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{     

					if (errornum==6) 
					{
						//6 -> (1) Session token expired, please login.
						startLogin();
					}
					else if (errornum==7) 
					{
						//7 -> (1) Invalid token, please login or register.
						startLogin();
					}
					dialog.dismiss();
				}
			});

			builder.setNegativeButton(getResources().getString(R.string.cancelbutton),
					new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					// Intent intent=new Intent(SignUpOne.this, MainActivity.class);
					// startActivity(intent);
					dialog.dismiss();
				}
			});


			AlertDialog	 alert = builder.create();
			alert.setCancelable(false);
			alert.show();
		}

		private void ErrorMessage(String title,String message,final boolean flageforSwithchActivity)
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle(title);
			builder.setMessage(message);

			builder.setPositiveButton(getResources().getString(R.string.okbuttontext),
					new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					if (flageforSwithchActivity) 
					{
						Intent intent=new Intent(getActivity(), MainActivity.class);
						startActivity(intent);
						getActivity().finish();
					}
					else
					{
						// only show message  
					}
					dialog.dismiss();
				}
			});

			AlertDialog	 alert = builder.create();
			alert.setCancelable(false);
			alert.show();
		}

	}


	private class BackGroundTaskFoResetPassword extends android.os.AsyncTask<String, Void, ResetPassword>
	{
		private Utility utility=new Utility();
		private List<NameValuePair>resetPasswordParamList;
		private String resetPasswordResponse;
		private ResetPassword resetPassword;
		private boolean isSuccess=true;
		private  ProgressDialog mdialog;

		@Override
		protected ResetPassword doInBackground(String... params) 
		{
			resetPasswordParamList=utility.getResetPasswordParameter(params);
			resetPasswordResponse=  utility.makeHttpRequest(VariableConstants.Resetpassword_url,VariableConstants.methodeName,resetPasswordParamList);
			//Log.d(TAG, "BackGroundTaskFoResetPassword doInBackground resetPasswordResponse "+resetPasswordResponse);
			if (resetPasswordResponse!=null)
			{
				Gson gson = new Gson();
				resetPassword=gson.fromJson(resetPasswordResponse, ResetPassword.class);
			}
			else 
			{
				isSuccess=false;
			}
			return resetPassword;
		}
		@Override
		protected void onPreExecute()
		{
			super.onPreExecute();
			mdialog=Utility.GetProcessDialog(getActivity());
			mdialog.setMessage(getResources().getString(R.string.Pleasewaitmessage));
			mdialog.show();
		}
		@Override
		protected void onPostExecute(ResetPassword result)
		{
			super.onPostExecute(result);
			if (mdialog!=null)
			{
				mdialog.dismiss();
				mdialog.cancel();
				mdialog=null;
			}
			//	Error Codes:
			//ErrorNumber -> (ErrorFlag) ErrorMessage
			if (isSuccess) 
			{
				if (result.getErrFlag()==0&&result.getErrNum()==67) 
				{
					///67 -> (0) Reset password instructions are sent to your registered mail, please follow them.
					succesMessage(getResources().getString(R.string.messagetitle),result.getErrMsg(),true);
					//startLogin();

				}
				else if (result.getErrFlag()==1&&result.getErrNum()==1) 
				{
					//1 -> (1) Mandatory field missing
					ErrorMessage(getResources().getString(R.string.error),result.getErrMsg(),false);

				}
				else if (result.getErrFlag()==1&&result.getErrNum()==6) 
				{
					//6 -> (1) Session token expired, please login.
					ErrorMessageForSessionExpoired(getResources().getString(R.string.error),result.getErrMsg(),result.getErrFlag(),result.getErrNum());

				}
				else if (result.getErrFlag()==1&&result.getErrNum()==7) 
				{
					//7 -> (1) Invalid token, please login or register.
					ErrorMessageForSessionExpoired(getResources().getString(R.string.error),result.getErrMsg(),result.getErrFlag(),result.getErrNum());

				}
				else if (result.getErrFlag()==1&&result.getErrNum()==68) 
				{
					//68 -> (1) Unable to send email, please try after some time.

					ErrorMessage(getResources().getString(R.string.error),result.getErrMsg(),false);
				}
			}
			else 
			{
				// some serviere issuse accured
				ErrorMessage(getResources().getString(R.string.error),"Request timeout",false);

			}


		}
		@Override
		protected void onCancelled()
		{
			super.onCancelled();
		}
		@Override
		protected void onCancelled(ResetPassword result) 
		{
			super.onCancelled(result);
		}



		private void startLogin()
		{
			SessionManager sessionManager=new SessionManager(getActivity());
			sessionManager.logoutUser();
			Intent intent=new Intent(getActivity(), SplahsActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			getActivity().finish();
		}


		private void ErrorMessageForSessionExpoired(String title,final String message,final int errorFlag,final int errornum)
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle(title);
			builder.setMessage(message);

			builder.setPositiveButton(getResources().getString(R.string.okbuttontext),
					new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{     

					if (errornum==6) 
					{
						//6 -> (1) Session token expired, please login.
						startLogin();
					}
					else if (errornum==7) 
					{
						//7 -> (1) Invalid token, please login or register.
						startLogin();
					}
					dialog.dismiss();
				}
			});

			builder.setNegativeButton(getResources().getString(R.string.cancelbutton),
					new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					// Intent intent=new Intent(SignUpOne.this, MainActivity.class);
					// startActivity(intent);
					dialog.dismiss();
				}
			});


			AlertDialog	 alert = builder.create();
			alert.setCancelable(false);
			alert.show();
		}

		private void ErrorMessage(String title,String message,final boolean flageforSwithchActivity)
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle(title);
			builder.setMessage(message);

			builder.setPositiveButton(getResources().getString(R.string.okbuttontext),
					new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					if (flageforSwithchActivity) 
					{
						Intent intent=new Intent(getActivity(), MainActivity.class);
						startActivity(intent);
						getActivity().finish();
					}
					else
					{
						// only show message  
					}
					dialog.dismiss();
				}
			});

			AlertDialog	 alert = builder.create();
			alert.setCancelable(false);
			alert.show();
		}

		private void succesMessage(String title,String message,final boolean flageforSwithchActivity)
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle(title);
			builder.setMessage(message);

			builder.setPositiveButton(getResources().getString(R.string.okbuttontext),new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					if (flageforSwithchActivity) 
					{
						startLogin();
					}
					else
					{
						// only show message  
					}
					dialog.dismiss();
				}
			});

			AlertDialog	 alert = builder.create();
			alert.setCancelable(false);
			alert.show();
		}

	}


}
