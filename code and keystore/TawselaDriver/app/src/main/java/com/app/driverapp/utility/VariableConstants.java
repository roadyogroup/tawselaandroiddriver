package com.app.driverapp.utility;

public class VariableConstants 
{
	//public static final String hostUrl="http://postmenu.cloudapp.net/Tawsela/services.php/";
	//public static final String ImageUrl="http://postmenu.cloudapp.net/Tawsela/pics/";
	
	public static final String hostUrl="http://45.55.70.196/Tawsela/services.php/";
	public static final String ImageUrl="http://45.55.70.196/Tawsela/pics/";
	
	/**
	 * Needs to change file name
	 */
	//File name for profile picture during Sign-up
	public static final String TEMP_PHOTO_FILE_NAME = "temp_pic.jpg";
	public static final String PROJECT_ID="721430009466";
	
	public static final String TERMSLINK="http://45.55.70.196/Tawsela/terms_of_use.php";
	public static final String PRIVACYLINK="http://45.55.70.196/Tawsela/privacy_policy.php";
	
	public static final String GET_COMPANY_TYPE=hostUrl+"getTypes";
	public static final String MASTER_SIGNUP_STEP_1 =hostUrl+"masterSignup1";
	public static final String getAppointmentDetail_url=hostUrl+"getMasterAppointments";
    public static final String uploadImage_url=hostUrl+"uploadImage";
    public static final String getRejectstatusUpdate_url=hostUrl+"respondToAppointment";
    public static final String getAppointmenttatus_url=hostUrl+"getApptStatus";
	public static final String getAppointmentstatusUpdate_url=hostUrl+"updateApptStatus";
	public static final String getUpdateAppointmentDetail_url=hostUrl+"updateApptDetails";
	public static final String getAppointmentDetailhistory_url=hostUrl+"getHistoryWith";
	public static final String getAbortJourney_url=hostUrl+"abortJourney";
	public static final String getProfileinfo_url=hostUrl+"getMasterProfile";
	public static final String logOut_url=hostUrl+"logout";
	public static final String Resetpassword_url=hostUrl+"resetPassword";
	public static final String Forgotpassword=hostUrl+"forgotPassword";
	public static final String getAppointmentDetails_url=hostUrl+"getAppointmentDetails";
	public static final String getMasterStatus_url=hostUrl+"updateMasterStatus";
	public static final String getMasterLocation_url=hostUrl+"updateMasterLocation";
	public static final String getPandingBooking_url=hostUrl+"getPendingRequests";
	public static final String getupdatelanguge_url = hostUrl+"updateLanguage";
	public static final String methodeName="POST";
	public static final String PREF_NAME = "DriverApp";
	public static final String PASSENGERAPPOINMENTDATEANDTIME="passengerappointmentdateandtime"; 
	public static final String APPOINTMENT="appointment"; 
	public static final String LOGINERRORFLAG="loginerrorflag";
	public static final String LOGINERRORNUM="loginerrornum";
	public static final String LOGINERRORMESSAGE="loginerrormessage";
	public static final String USERTYPE="1";
	public static final String FLLURY_ANALYTICS_ID="T9DR5YFK3P3W8YXJHGBJ";
	
	public static int updatelocationUpdateTimerTiem = 2000;// 2 second
	
	public static final String CURRENCY = "AED";
	
	
}
