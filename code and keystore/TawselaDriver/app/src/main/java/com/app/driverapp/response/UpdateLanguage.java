package com.app.driverapp.response;

import com.google.gson.annotations.SerializedName;

public class UpdateLanguage 
{
	@SerializedName("errNum")
	private String errNum;
	@SerializedName("errFlag")
	private  String errFlag;
	@SerializedName("errMsg")
	
	private String errMsg;
	public String getErrNum() {
		return errNum;
	}
	public void setErrNum(String errNum) {
		this.errNum = errNum;
	}
	public String getErrFlag() {
		return errFlag;
	}
	public void setErrFlag(String errFlag) {
		this.errFlag = errFlag;
	}
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
}
