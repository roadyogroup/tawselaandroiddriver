package com.app.driverapp.utility;

import java.util.Hashtable;

import org.json.JSONArray;
import org.json.JSONObject;
import com.pubnub.api.Callback;
import com.pubnub.api.Pubnub;
import com.pubnub.api.PubnubError;

public class PublishUtility 
{
	
	public static void publish(String channelName,String Message ,Pubnub pubnub) 
	{
		_publish(channelName,Message,pubnub);
	}
	
	public static void _publish(final String channel,String message,Pubnub pubnub) 
	{
		
		Hashtable<String, Object> args = new Hashtable<String, Object>(2);
		if (args.get("message") == null)
		{
			try {
				Integer i = Integer.parseInt(message);
				args.put("message", i);
			} catch (Exception e) {

			}
		}
		if (args.get("message") == null) {
			try {
 				Double d = Double.parseDouble(message);
				args.put("message", d);
			} catch (Exception e) {

			}
		}
		if (args.get("message") == null) {
			try {
				JSONArray js = new JSONArray(message);
				args.put("message", js);
			} catch (Exception e) {

			}
		}
		if (args.get("message") == null) 
		{
			try {
				JSONObject js = new JSONObject(message);
				args.put("message", js);
			} catch (Exception e) {

			}
		}
		if (args.get("message") == null) 
		{
			args.put("message", message);
		}

		args.put("channel", channel); // Channel Name

		pubnub.publish(args, new Callback() 
		{
			@Override
			public void successCallback(String channel,	Object message) 
			{
				notifyUserforPublish("PUBLISH : " + message);
			}
			@Override
			public void errorCallback(String channel, PubnubError error)
			{
				notifyUserforPublish("channel name : " + channel);
				notifyUserforPublish("PUBLISH : " + error);
			}
		});
	}
	
	private static void notifyUserforPublish(final String msg)
	{
		Utility.printLog("MainActivityDrower ", "notifyUserforBublish  msg "+msg);
	}
	
}
